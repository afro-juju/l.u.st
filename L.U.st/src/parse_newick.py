"""                                                                                                                                                                                  
Created by Wasiu Ajenifuja Akanni on 2013.                                                                                                                                           
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.                                                                                                                      
"""

import string
import re
class parse_newick:
    
    def parse_newick(self,arg1):
#        print arg1
        tree1 = re.sub(r'[\):][0-9]+\.?[0-9]+([,\)])',r'\1',arg1) # remove all branch lenghts and support values   
#        tree1 = re.sub(r'[\):][0-9]+\.?([,\)])',r'\1',arg1) # remove all branch lenghts and support values
        tree2 = re.sub(r'(\))[0-9]+\.?([,:\)])',r'\1\2',tree1)
 #       print tree1
        tree3 = re.sub(r'(\))[0-9]+\.?([,:\)])',r'\1\2',tree2)
        tree3 = re.sub(r'\s', '', tree3) # remove all white space characters 
        l = list(tree3)
        j = []
        word = ''

        for i in l:
            if i == '(':
                if word != '':
                    j.append(word)
                word = ''
                j.append(i)
            elif i == ',':
                if word != '':
                    j.append(word)
                word = ''
                j.append(i)
            elif i == ')':
                if word != '':
                    j.append(word)
                word = ''
                j.append(i)
            else:
                word = word + i
        return j
