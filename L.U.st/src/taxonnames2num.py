"""                                                                                                                                                                                  
Created by Wasiu Ajenifuja Akanni on 2013.                                                                                                                                           
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.                                                                                                                      
"""

import parse_newick

def change_taxonname2num(in_list, taxon_list):
# this method uses the alltaxon list to change the taxon names in their trees to the repective list index
    input_list= []# this list contains the input trees now represent by the list index of the repective taxons 

    parser = parse_newick.parse_newick() # create an object of the parse_newick class

    for i in in_list:
        word = ''
        tree = parser.parse_newick(i)
        for j in tree:
            k = j
            if k.lower()in map(str.lower,taxon_list):
                k = taxon_list.index(k.lower())
            word = word + str(k)    
        input_list.append(word)
    return input_list
