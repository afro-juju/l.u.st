"""                                                                                                                                                                                  
Created by Wasiu Ajenifuja Akanni on 2013.                                                                                                                                           
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.                                                                                                                      
"""

import Treecopy2
import Tree_Splits
class sym_diff:
    
    def get_sym_diff(self,tree1,tree2):
#        print 'start sym diff calculations***************************************'
        t1 = Tree_Splits.Splits()
        t2 = Tree_Splits.Splits()
        t1_split = t1.get_splits(tree1)
        t2_split = t2.get_splits(tree2)
        t1_split_nu = []
        t2_split_nu = []

        f= 0
        while f < len(t1_split):
            current_t1split = t1_split[f]
#            print 'this is current ' + str(f) + current_t1split
#            if current_t1split not in t2_split:
            half_first = current_t1split.split("|")[0]
            #            half_current_list = list(half_current)
            half_first_ob = Treecopy2.Tree(half_first,'h')                                             
            half_dic = half_first_ob.container                                                            
            no_first_taxa = 0  
            first_taxa = []
            for key in half_dic.keys():
                if half_dic[key].id != None:
                    no_first_taxa +=1
                    first_taxa.append(half_dic[key].id)
            present = 'no'    
            j= 0
            while j < len(t2_split):
#                print ' we are comparing current to this  and j = :' + str(j)
                current_t2list= t2_split[j]
#                print current_t2list
                half_second = current_t2list.split("|")[0]
                half_second_ob = Treecopy2.Tree(half_second,'s')
                half_seconddic = half_second_ob.container
                no_second_taxa = 0
                second_taxa = []
                for key in half_seconddic.keys():
                    if half_seconddic[key].id != None:
                        no_second_taxa +=1
                        second_taxa.append(half_seconddic[key].id)
                if no_first_taxa == no_second_taxa:
                    first_taxa.sort()
                    second_taxa.sort()                    
                    if first_taxa==second_taxa:
                        present = 'yes'
#                        print 'we are passing '
                if present =='no':
                    half2_second = current_t2list.split("|")[1]
                    half2_second_ob = Treecopy2.Tree(half2_second,'s')
                    half2_seconddic = half2_second_ob.container
                    no_second_taxa2 = 0
                    second_taxa2 = []
                    for key in half2_seconddic.keys():
                        if half2_seconddic[key].id != None:
                            no_second_taxa2 +=1
                            second_taxa2.append(half2_seconddic[key].id)
                    if no_first_taxa == no_second_taxa2:
                        first_taxa.sort()
                        second_taxa2.sort()
                        if first_taxa==second_taxa2:
                            present = 'yes'
#                        print 'we are passing '                                                                                                              


                if present == 'yes':
                    break
                j +=1
#           print present
            if present == 'no':
                t1_split_nu.append(current_t1split)


            f +=1        
        k = 0 
        while k < len(t2_split):
#            print 'this is current 2 : ' 
            current_t2split = t2_split[k]
#            if current_t2split not in t1_split:
            half_first = current_t2split.split("|")[0]
            #            half_current_list = list(half_current)                                             
            half_first_ob = Treecopy2.Tree(half_first,'h')
            half_dic = half_first_ob.container                                                          
            
            no_first_taxa = 0                                                                           
            first_taxa = []
            for key in half_dic.keys():
                if half_dic[key].id != None:
                    no_first_taxa +=1
                    first_taxa.append(half_dic[key].id)
#            print first_taxa
            present1 = 'no'
            p=0
            while p < len(t1_split):
                current_t1list= t1_split[p]
#                print ' this is what we are comparing current 2 to: ' + current_t1list
                half_second = current_t1list.split("|")[0]
                half_second_ob = Treecopy2.Tree(half_second,'s')
                half_seconddic = half_second_ob.container
                no_second_taxa = 0
                second_taxa = []
                for key in half_seconddic.keys():
                    if half_seconddic[key].id != None:
                        no_second_taxa +=1
                        second_taxa.append(half_seconddic[key].id)
                if no_first_taxa == no_second_taxa:
                    first_taxa.sort()
                    second_taxa.sort()
                    if first_taxa==second_taxa:
                        present1 = 'yes'
#                        print 'we are passing '
                if present1 == 'no':
                    half_second2 = current_t1list.split("|")[1]
                    half_second_ob2 = Treecopy2.Tree(half_second2,'s')
                    half_seconddic2 = half_second_ob2.container
                    no_second_taxa2 = 0
                    second_taxa2 = []
                    for key in half_seconddic2.keys():
                        if half_seconddic2[key].id != None:
                            no_second_taxa2 +=1
                            second_taxa2.append(half_seconddic2[key].id)
                    if no_first_taxa == no_second_taxa2:
                        first_taxa.sort()
                        second_taxa2.sort()
                        if first_taxa==second_taxa2:
                            present1 = 'yes'
#                        print 'we are passing '                                                                                                              
                if present1 == 'yes':
                    break
                p +=1
#            print present1
            if present1 == 'no':
                t2_split_nu.append(current_t2split)
            k +=1
#        print t1_split_nu
#        print t2_split_nu
#        print len(t1_split_nu)
#        print len(t2_split_nu)
        sym_diff = len(t1_split_nu) + len(t2_split_nu)
#        print sym_diff
#        print 'end of sym_diff*********************************************************'
        return sym_diff
