"""                                                                                                                                                                                  
Created by Wasiu Ajenifuja Akanni on 2013.                                                                                                                                           
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.                                                                                                                      
"""
import random
import re
import Treecopy2
class Node:
    """ A single node. """
    rerooted = ''
    all_children = {}
    as_string = ''
    as_string1 = ''
    nu_tree = ''
    def __init__(self):
        self.id = None
        self.N_Sibling = {}
        self.P_Sibling = {}
        self.daughters = {}
        self.parent = {}
        self.node_support = 0
        self.branch_lenght = 0
#        self.children = self.get_child()
#        self.test = {}
#        self.all_daughters = children

    def set_id(self,id):
        """This sets the id of a node, only when not set yet: (self,id)."""
        if self.id is not None:
            raise NodeException, 'You can not change a Nodes Id .'
        self.id=id

    def get_id(self):
        """Returns the node's id of self."""
        return self.id

    def set_P_Sibling (self, sibling):
        """sets the previous sibling"""
        self.P_Sibling = sibling

    def get_P_Sibling (self):
        """returns the previous sibling"""
        return  self.P_Sibling
        

    def set_N_Sibling (self, sibling):
        """sets the next sibling"""
        self.N_Sibling = sibling

    def get_N_Sibling (self):
        """returns the next sibling"""
        return  self.N_Sibling


    def get_child(self):
        """Returns a list of the node's successors."""
        Node.all_children = {}
        self.get_child2()
        print 'first all children'

        print Node.all_children
        return Node.all_children


    def get_child2(self):
        """Returns a list of the node's successors."""
        for key in self.daughters:

            self.daughters[key].get_child2()
            Node.all_children[key] = self.daughters[key]
            print 'second all children'
            print Node.all_children
        return Node.all_children

    def get_as_string(trey):
        Node.as_string = ''
        f = '('
        g = ')'
        k = ','
        s = ' '

        if trey.daughters == {}:
#            Node.as_string = s.join([Node.as_string,f])
            Node.as_string =s.join([Node.as_string,trey.id])
#            Node.as_string = s.join([Node.as_string,g])
        else:
            trey.get_as_string2()
        return Node.as_string.replace(' ','')

    def get_as_string2(trey):
        f = '('
        g = ')'

        k = ','
        s = ' ' 
        if trey.daughters != {}:
            Node.as_string= s.join([Node.as_string,f])
        j = len(trey.daughters)
#        print "DEBUG:NODE: get_as_string2: this is j : " , j
        i =1
        for key in sorted_nicely(trey.daughters.keys()):
            trey.daughters[key].get_as_string2()
            if trey.daughters[key].id != None:
                Node.as_string = s.join([Node.as_string,trey.daughters[key].id])
            if i <j:
                Node.as_string =s.join([Node.as_string,k])
                i += 1
        if trey.daughters != {}:
            Node.as_string = s.join([Node.as_string,g])
#        print "DEBUG:NODE: get_as_string2: this is node.as_string : " , Node.as_string
        
        
        return Node.as_string

    def set_daughters(self,new_daughter):
        """Sets the node's successors:"""
        if not isinstance(new_daughter,type([])):
            raise NodeException, 'Node successor must be of list type.'
        self.daughters=new_daughter

    def remove_children(self):
        """Removes all the node's successors:"""
        for key in self.daughters:
            self.daughters[key].remove_children()
            print key
            print 'will now be deleted'
            del self.daughters[key]
            
        
    def get_chopped(start,cutoff):
        Node.as_string1 = ''
        f = '('
        g = ')'
        k = ','
        s = ' '

        if start.daughters == {}:
#            Node.as_string1 = s.join([Node.as_string1,f])
            Node.as_string1 =s.join([Node.as_string1,start.id])
#            Node.as_string1 = s.join([Node.as_string1,g])
        else:
            start.get_as_chopped2(cutoff)
        return Node.as_string1.replace(' ','')

    def get_as_chopped2(start,cutoff):
        f = '('
        g = ')'
        k = ','
        s = ' '
        presence = 'No'
        n_daughter = len(start.daughters)
        if cutoff in start.daughters.keys():
            if n_daughter == 2: 
                presence = 'yes' 

        if start.daughters != {} and presence == 'No' :
            Node.as_string1= s.join([Node.as_string1,f])
        j = len(start.daughters)
        i =1

        for key in sorted_nicely(start.daughters.keys()):
#            if key == cutoff:
#                pass
            prin = 'Yes'
            if start.daughters[key].id != None and key != cutoff:
                Node.as_string1 = s.join([Node.as_string1,start.daughters[key].id])
            else:
                if start.daughters[key]== cutoff:
                    prin = 'No'
#            if i <j and prin == 'Yes':
#                Node.as_string1 =s.join([Node.as_string1,k])
#                i += 1
            if key != cutoff:
                start.daughters[key].get_as_chopped2(cutoff)
            if i <j and prin == 'Yes' and key != cutoff and presence == 'No':
                Node.as_string1 =s.join([Node.as_string1,k])
            i += 1

        if start.daughters != {} and presence == 'No':
            Node.as_string1 = s.join([Node.as_string1,g])
#        print Node.as_string1


        return Node.as_string1

    def TBR1(trey,root,cut):
        t1 = trey.get_as_string()
        print 'this is t1'
        print t1
        t2 = root.get_chopped(cut)
        print 'this is t2'
        print t2

    def SPR(prun_pt,root,chopped_pt):
        insert = prun_pt.get_as_string()
        base = root.get_chopped(chopped_pt)
        t4 = Treecopy2.Tree(base,'n')
        t1 = t4.container
        key_list = t1.keys()
        for i in key_list:
            if i != 'Root'and i != 'n0':
                insert_pt = t1[i].get_as_string()
                rest = t1['Root'].get_chopped(i)
                print '((' + insert_pt + ','+ insert + ')'+','+ rest + ')'

    def TBR(prun_pt,root,chopped_pt):
        insert = prun_pt.get_as_string()
        base = root.get_chopped(chopped_pt)
        insert_tree = Treecopy2.Tree(insert, 'i')
        insert_dic = insert_tree.container
        base_tree = Treecopy2.Tree(base,'b')
        base_dic = base_tree.container
        base_keys = base_dic.keys()
        insert_keys = insert_dic.keys()
        short_list = []
        shorter_list= []
        count = 0
        for i in base_keys:
            if i != 'Root' and i != 'b0':
#                base_rerooted = base_dic[i].Reroot()
#                base_rerooted_tree = Treecopy2.Tree(base_rerooted,'br')
#                base_rerooted_dic = base_rerooted_tree.container
#                base_rerooted_keys = base_rerooted_dic.keys()
#                for k in base_rerooted_keys:
#                    if k != 'Root' and k != 'br0':
                for j in insert_keys:
                    if j != 'Root' and j != 'i0':
                        insert_rerooted = insert_dic[j].Reroot()
#                        print 'this is the insert rerooted at '+ j+ ' :' + insert_rerooted
                        insert_pt = base_dic[i].get_as_string()
                        rest = base_dic['Root'].get_chopped(i)
#                        print 'wee are inserting the rerooted tree into position ' + i 
#                        print '((' + insert_pt + ',' + insert_rerooted + ')' + ',' + rest + ')'
                        new_topology = '((' + insert_pt + ',' + insert_rerooted + ')' + ',' + rest + ')'
                        short_list.append(new_topology)
            while count < 3:
                i_list = []
                i =  random.randrange(0,len(short_list)+1)
                if i != len(short_list)+1 and i not in i_list:
                    i_list.append(i)
                    shorter_list.append(short_list[i])
                    count +=1
                        
            return shorter_list

    def Reroot(nu_root):
        Node.rerooted = ''
        f = '('
        g = ')'
        k = ','
        s = ''

        Node.rerooted = s.join([Node.rerooted,f])
        if nu_root.daughters == {}:
            Node.rerooted = s.join([Node.rerooted,nu_root.id])
        else:
            all_sibling = nu_root.get_as_string()
#            print 'all ' + all_sibling
            Node.rerooted = s.join([Node.rerooted,all_sibling])
        count = 1    
        nu_root.Reroot2(count)
        return Node.rerooted.replace(' ','')
    def Reroot2(nu_root,count):
        f = '('
        g = ')'
        k = ','
        s = ''
        prin = 'no'
        count = count
#        print 'this is count'
#        print count
        for i in nu_root.parent.keys():
#            print 'parent' + i
            if nu_root.parent[i].parent != {}:
                
                if nu_root.N_Sibling != {}:
                    nxt_sibling = ''
#                    print 'yes nxt sibling exist'
                    for i in nu_root.N_Sibling.keys():
#                        print i
                        Node.rerooted = s.join([Node.rerooted,k])
                        Node.rerooted = s.join([Node.rerooted,f])
                        count +=1
                        if nu_root.N_Sibling[i].daughters == {}:
                            Node.rerooted = s.join([Node.rerooted,nu_root.N_Sibling[i].id])
                        else:
                            all_sibling = nu_root.N_Sibling[i].get_as_string()
#                            print 'all ' + all_sibling
                            Node.rerooted = s.join([Node.rerooted,all_sibling])
#                        print Node.rerooted

                else:
                    prev_sibling = ''
                    for i in nu_root.P_Sibling.keys():
                        Node.rerooted = s.join([Node.rerooted,k])
                        Node.rerooted = s.join([Node.rerooted,f])
                        count +=1
                        if nu_root.P_Sibling[i].daughters == {}:
                            Node.rerooted = s.join([Node.rerooted,nu_root.P_Sibling[i].id])
                        else:
                            all_sibling = nu_root.P_Sibling[i].get_as_string()
                            Node.rerooted = s.join([Node.rerooted,all_sibling])
#                        print Node.rerooted
                

        

                parent = ''
                if nu_root.parent != {}:
                    for i in nu_root.parent.keys():
#                        print 'parent' + i 
                        nu_root.parent[i].Reroot2(count)
            else:
                
                if nu_root.N_Sibling != {}:
                    nxt_sibling = ''
#                    print 'yes nxt sibling exist'
                    for i in nu_root.N_Sibling.keys():
                        Node.rerooted = s.join([Node.rerooted,k])
                        if nu_root.N_Sibling[i].daughters == {}:
                            Node.rerooted = s.join([Node.rerooted,nu_root.N_Sibling[i].id])
                        else:
                            all_sibling = nu_root.N_Sibling[i].get_as_string()
#                            print 'all ' + all_sibling
                            Node.rerooted = s.join([Node.rerooted,all_sibling])
                        while count > 0:
                            Node.rerooted = s.join([Node.rerooted,g])
                            count -=1
                                                   
#                        print Node.rerooted
                        
                else:
                    prev_sibling = ''
                    for i in nu_root.P_Sibling.keys():

                        Node.rerooted = s.join([Node.rerooted,k])
                        
                        if nu_root.P_Sibling[i].daughters == {}:
                            Node.rerooted = s.join([Node.rerooted,nu_root.P_Sibling[i].id])
                        else:
                            all_sibling = nu_root.P_Sibling[i].get_as_string()
                            Node.rerooted = s.join([Node.rerooted,all_sibling])
                        while count > 0:
                            Node.rerooted = s.join([Node.rerooted,g])
                            count -=1


#                        print Node.rerooted
                        

                        
            return Node.rerooted

def sorted_nicely( l ): 
    """ Sort the given iterable in the way that humans expect.""" 
    convert = lambda text: int(text) if text.isdigit() else text 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)
