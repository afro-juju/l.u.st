"""                                                                                                                                                                               
Created by Wasiu Ajenifuja Akanni on 2013.                                                                                                                                        
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.                                                                                                                   
"""
import parse_newick
def maktaxon_list(in_list):
#this method creates a list of all the taxon sets from the input trees
    alltaxon_set= [] # this is where the taxon_set of the input trees are to be stored and their index are to used to represent them from now on.. 
    parser = parse_newick.parse_newick() # create an object of the parse_newick class

    for i in in_list:
        tree = parser.parse_newick(i)
        for i in tree:
            if i not in ['(',')',',']:
                if i.lower() not in  map(str.lower,alltaxon_set):
                    alltaxon_set.append(i)

    return alltaxon_set
