"""                                                                                                                                                                                  
Created by Wasiu Ajenifuja Akanni on 2013.                                                                                                                                           
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.                                                                                                                      
"""

import Tree_Splits
import Node
import string
import parse_newick
class Tree:
    def __init__(self,tree_in,padding):
#        print padding
        t1 = parse_newick.parse_newick()
        nuformat =t1.parse_newick(tree_in)
#        print nuformat
        node_dict ={}
        result = self.tree_build(padding,nuformat,node_dict,list_index=0,parent =None)
        node_dict['Root'] = node_dict[padding+str(0)]
#        print node_dict
        self.container = node_dict
    def tree_build(self,padding,nuformat,node_dict,list_index, parent):
#        print 'nw in the tree build'
        previous_sibling = None
        
        while ((list_index < len(nuformat))and(nuformat[list_index]!= ')')):
#            print 'now in the while'
#            print list_index
            if nuformat[list_index] =='(':
                node = Node.Node()
                dic_key = padding + str(len(node_dict))
                node_dict[dic_key] = node
                
                
#                print ' i = (...................'
#                print parent
                if previous_sibling:
#                    print ' yes prev.siblin exist'
                    node.P_Sibling[padding+str(previous_sibling)] = node_dict[padding+str(previous_sibling)]
                    node_dict[padding+str(previous_sibling)].N_Sibling[padding+str(len(node_dict)-1)] = node_dict[padding+str(len(node_dict)-1)]
                previous_sibling =len(node_dict) - 1
                if parent != None:
#                    print 'yes parent exist'
                    node.parent[padding+str(parent)] = node_dict[padding+str(parent)]
                    node_dict[padding+str(parent)].daughters[padding+str(len(node_dict)-1)] = node_dict[padding+str(len(node_dict)-1)]

                list_index = self.tree_build(padding,nuformat, node_dict,list_index +1,len(node_dict)-1)
#                print 'dont with ('
#                print 'anoder list index'
#                print list_index
                pass

            elif nuformat[list_index] == ',':
                list_index += 1
#                print 'we a nw lookin , '
                pass
            elif nuformat[list_index] == ')':
#                print 'we a nw lookin ) '
                pass
            elif nuformat[list_index] == ';':
#                print 'we a nw lookin , '
                pass
    
            else:
#                print 'we a nw lookin names '
                node = Node.Node()
#                dict_lenght = len(node_dict)
                node_dict[padding+str(len(node_dict))] = node
                name = nuformat[list_index]
#                print name
                node.id = name
                node.parent[padding+str(parent)] = node_dict[padding+str(parent)]

#                print parent
                if previous_sibling:
#                    print 'yes prev exit'
                    node.P_Sibling[padding+str(previous_sibling)] = node_dict[padding+str(previous_sibling)]
#                    print previous_sibling
                    node_dict[padding+str(previous_sibling)].N_Sibling[padding+str(len(node_dict)-1)] = node_dict[padding+str(len(node_dict)-1)]
                previous_sibling =len(node_dict) - 1
#                print parent
                if parent != None:
#                    print 'yes parant exist'
                    node_dict[padding+str(parent)].daughters[padding+str(len(node_dict)-1)] = node_dict[padding+str(len(node_dict)-1)]
                list_index +=1
#                print 'list index after a name'
#                print list_index
                
        return list_index +1
    def get_TreeDict(self):
        return self.container
                
    def showtree(self,tree_dict = {}):
        for key in tree_dict:
            tree_dict[key].get_id()
            tree_dict[key].get_N_Sibling()
            tree_dict[key].get_N_Sibling()
            tree_dict[key].get_daughters()

    

