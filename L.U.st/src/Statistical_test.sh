PATH=..:../src:../bin:$PATH
"""
Created by Wasiu Ajenifuja Akanni on 2013.
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.
"""
echo "This script along with the Statistical_test.py and the CONSEL software are you used to perform test such as the AU,SH,KH test for competing hypothesis "
echo "NOTE: when giving input files, do not put them within quotes"

 
echo "Please give the name the file containing your list of supertree: "
read super_file
echo "Please give the name of the file containing your list of input trees: "
read input_file
echo "Please give the name of the file where the input like value of supertrees is to be stored: *this must end in .txt"
read out_file
echo "Please give the name of the file that will contain the statistic results: "
read stat_file
echo "Please give the number of replicate you would like in multiples of 10. The default is 100000, so if you put 10 it will do 1000000 replicates: "
read r
super_file1=$(echo "$super_file" | sed "s/^/\'/" | sed "s/$/\'/")
input_file1=$(echo "$input_file" | sed "s/^/\'/" | sed "s/$/\'/")
out_file1=$(echo "$out_file" | sed "s/^/\'/" | sed "s/$/\'/")
echo out_file1
python Statistical_test.py <<EOF
$super_file1
$input_file1
$out_file1
EOF

makermt --paup $out_file -b $r
new_file=$(echo "$out_file" | sed 's/.txt//')
consel $new_file
catpv -v -e -s 9 $new_file >$stat_file
rm *.pv
rm *.ci
rm *.vt
rm *.rmt
echo 'DONE!!!!!!!!!!'