from __future__ import division
import math
import Treecopy2
import Prune
import Sym_diff
class Supertree_likelihood:

    def get_superlik(self,input_treeDic, supertree):
#        print 'now in the get_superlik'
        input_list = input_treeDic.keys()
        input_counter = 0
        counter = len(input_list)
        likez = []
        dist =[]
        non_normalised_dist = []
        while input_counter < counter:
#            print 'DEBUG:Supertree_likelihood: now in the while loop'
            input_tree = input_list[input_counter]
            input_treeTaxa = input_treeDic[input_tree]
            no_of_inputtaxa = len(input_treeTaxa)
            prune_object = Prune.Prune()

#            print 'DEBUG:Supertree_likelihood: this is the supertree we are sending in ' + supertree 
            pruned_supertree = prune_object.Prune(input_tree,input_treeTaxa,supertree)

# THIS NEXT CODES REROOTS THE PRUNED SUPERTREE ON THE SAME TAXA THAT THE INPUT TREE AS BEEN ROOTED ON. THIS IS JUST AN ADDED INSURANCE FOR THE SYMMETRIC DIFF ANALYSIS.
            pruned_stree1= Treecopy2.Tree(pruned_supertree, 'W')
            s_dic = pruned_stree1.container
            for w in s_dic.keys():
                if s_dic[w].id == input_treeTaxa[0]:
                    s_root_pt = w
                    break
            pruned_stree_rerooted = s_dic[s_root_pt].Reroot()
#            print 'DEBUG:Supertree_likelihood_rSPR: this is the input tree, rerooted pruned supertree ',input_tree, "    ", input_treeTaxa ,"   " ,  pruned_stree_rerooted 

            rf_object = Sym_diff.sym_diff()
            distance = rf_object.get_sym_diff(input_tree,pruned_stree_rerooted)

#            normal_dist = distance/((2* no_of_inputtaxa)-6)
#            normal_dist = normal_dist + 1
#            normal_dist = normal_dist * -1
#            print 'this is distance '                                                                                                                                                                                                
#            print distance                                                                                                                                
            alpha = 1
            beta = 1

            like = get_like(alpha,beta,distance)
#        print like                                                                                                                                    

            likez.append(like)
#            dist.append(normal_dist)
            non_normalised_dist.append(distance)

            input_counter +=1

        like_current_supertree = sum(likez)

 #       sumdist = sum(dist)
 #       print sumdist
#        unsumdist = sum(non_normalised_dist)
#        print like_current_supertree
#        print '111111111111111111111111111111111111111111111111111111111111111111111111'
#        print 'end of get superlik'
        return like_current_supertree

def get_like(alpha,beta,d):

    delta = d   # Normalised tree-to-tree distance.  THis is currently Normalised RF but can take any distane (e.g. quartet)
                                                                                                                        
    bd = beta * delta * -1# remember the -1 might not work AJ.  Test it!                                         
#    print bd
    exp_bd = math.exp(bd)
#    print exp_bd
    like= (alpha * exp_bd)
#    print '############################### /n'
#    print like
    if like !=0:
        like = math.log10(like)
    else:
        print'####################0000000000000000000'
    return like
