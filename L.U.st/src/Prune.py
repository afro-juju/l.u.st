"""                                                                                                                                                                                  
Created by Wasiu Ajenifuja Akanni on 2013.                                                                                                                                           
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.                                                                                                                      
"""
import Treecopy2
import re
import parse_newick
import Node
class Prune:
    def Prune(self,tree1,_input_taxa, tree2):
#        print 'now in the Prune classs'
#        print tree1
#        print tree2
#        tree1A = re.sub(r'[\):][0-9]+\.?[0-9]+([,\)])',r'\1',tree1) # remove all branch lenghts and support values      
#        tree1B = re.sub(r'(\))[0-9]+\.?([,:\)])',r'\1\2',tree1A)
#        tree1 = tree1B
#        tree2A = re.sub(r'[\):][0-9]+\.?[0-9]+([,\)])',r'\1',tree2) # remove all branch lenghts and support values      
#        tree2B = re.sub(r'(\))[0-9]+\.?([,:\)])',r'\1\2',tree2A)
#        tree2 = tree2B
#        print tree1
#        print tree2
        input_tree_str = tree1
        input_taxa = _input_taxa
        super_tree_str = tree2
        taxa_2_prune = []
        super_tree = Treecopy2.Tree(super_tree_str,'s')
        super_tree_dic = super_tree.container
        for i in super_tree_dic.keys():
            if super_tree_dic[i].id != None:
                if super_tree_dic[i].id not in input_taxa:
                    taxa_2_prune.append(super_tree_dic[i].id)
#        print 'this are the taxa to prune'
#        print taxa_2_prune
        working_copy = tree2

        for i in taxa_2_prune:
#            print 'PRUNE DEBUG: this is taxa we want to prune ' +i + "\n"
#            print 'PRUNE DEBUG: This is working cp ' + working_copy
            if ',' in working_copy: #Added to ensure we dont get a error when deleting all last taxa left in a tree.
                working_tree = Treecopy2.Tree(working_copy, 'A')
                working_dic = working_tree.container
                for taxa in working_dic.keys():
                    #                print taxa
                    if working_dic[taxa].id == i.strip():
                        working_copy = working_dic['Root'].get_chopped(taxa)
                    #                    print working_copy
                        pass 
            else:
                working_copy = ''
                break
        working_copy= working_copy.replace(',)',')') #Added due to the get_chooped method---This replace statement was added by me because i found out that if the node containing the taxa to prune has 3 children(in the case of the root of fully bifurcating trees) a comma is added eg '),)'. i dont if theres a smarter way to deal with this yet 
        return working_copy
 
