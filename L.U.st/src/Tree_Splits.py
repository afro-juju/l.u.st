"""                                                                                                                                                                                  
Created by Wasiu Ajenifuja Akanni on 2013.                                                                                                                                           
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.                                                                                                                      
"""
import Treecopy2
import Prune
class Splits:
    def get_splits(self, tree1):
#        print 'DEBUG1!!! now in tree Split------------------------'
#        print 'DEBUG2!!! '+ tree1
        split_list = []
        trivial_list = []
        if ',' in tree1:
            tree = Treecopy2.Tree(tree1, 's') # creates a tree object
            tree_dic = tree.container # puts all the nodes on the tree into a dictionary
            split_list = []
            trivial_list = []
            present = ''
            for i in tree_dic.keys():
                working_copy = tree_dic
                if tree_dic[i].id == None and i != 'Root' and i != 's0':
                    first = tree_dic[i].get_as_string()
                    second = tree_dic['Root'].get_chopped(i)
                    if '(' in first and ')' in second:
                        first_tree_ob = Treecopy2.Tree(first,'f')
                        first_dic = first_tree_ob.container
                        no_of_firsttaxa = 0
                        for i in first_dic.keys():
                            if first_dic[i].id != None:
                                no_of_firsttaxa +=1
                        second_tree_ob = Treecopy2.Tree(second,'s')
                        second_dic = second_tree_ob.container
                        no_of_secondtaxa = 0
                        for i in second_dic.keys():
                            if second_dic[i].id != None:
                                no_of_secondtaxa +=1


                        if no_of_firsttaxa < no_of_secondtaxa:
                            d_split = first + '|' + second
                        else:
                            d_split = second + '|' + first
                        if d_split not in split_list:
                            i = 0
                            present = 'no'
                            jay = d_split.split("|")[0]
#                        print d_split
                            while i < len(split_list):
                                if present == 'no':
#                                print 'yay'
                                    if jay == split_list[i].split("|")[0]:
#                                    print jay
#                                    print split_list[i].split("|")[0]
                                        present = 'yes'
                                    if jay == split_list[i].split("|")[1]:
#                                    print jay
#                                    print split_list[i].split("|")[1]
                                        present = 'yes'
                                i +=1
                            if present == 'no':
                                split_list.append(d_split)
#                    print i
#                    print first + '|' + second
                    else:
                        trivial = first + '|' + second
                        trivial_list.append(trivial)
#                    print 'trivial' + trivial
        else:
            pass
#        print 'finished with tree split----------------------'
#        print split_list
        return split_list
                













