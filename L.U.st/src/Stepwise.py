"""                                                                                                                                                                                  
Created by Wasiu Ajenifuja Akanni on 2013.                                                                                                                                           
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.                                                                                                                      
"""
import random
class stepwise:
    tree = ''
#    print' start of stepwise************************************************************************'
    def get_stepwise_tree(self,li):
        
        f = '('
        g = ')'
        k = ','
        s = ' '
        li = li
        li_nu = []
        global tree
        #tree = ''
        tracker = []
#        print 'this is li '
#        print li
        if len(li) !=1:
            no_of_couples = len(li)/2
            j = 0
#            print no_of_couples
            while j != no_of_couples:
                tyms = 0
                couple = ''
                couple = s.join([couple,f])
                while tyms<2:
                    r = random.randrange(0,len(li))
#                    print r
                    if r not in tracker:
                        tracker.append(r)
                        couple = s.join([couple,li[r]])
                        if tyms == 0:
                            couple = s.join([couple,k])
                        tyms +=1
                couple = s.join([couple,g])
 #               print couple
                li_nu.append(couple)
                j +=1
            if len(li)%2 != 0:
                check = 0
                while check != len(li):
                    if check not in tracker:
                        li_nu.append(li[check])
                        break
                    check +=1
#            print 'this is li nu '
#            print li_nu
            tree = li_nu[0]
            t = stepwise()
            t.get_stepwise_tree(li_nu)
#        print 'this is last tree '
#        print tree
        tree = tree.replace(' ' , '')
#        print 'end of stepwise***********************************************************************'
        return tree
