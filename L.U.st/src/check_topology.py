"""                                                                                                                                                                                  
Created by Wasiu Ajenifuja Akanni on 2013.                                                                                                                                           
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.                                                                                                                      
"""
import Treecopy2
#checks if nu_tree topology is already a topology in mintree_list or not
def is_newtopology(nu_tree,mintree_list):
#    print 'start of check topology' + nu_tree
    same_topology = 'no'

    treeB = Treecopy2.Tree(nu_tree,'B')
    treeB_dic = treeB.container
    Bset_list = []
    for i in treeB_dic.keys():
        if i != "Root" and i != 'B0':
            if treeB_dic[i].id == None:
                B = treeB_dic[i].get_as_string()
                B_set = list(B)
                Bset_list.append(B_set)
    for j in mintree_list:
        if same_topology =='no':
            treeA = Treecopy2.Tree(j,'A')
            treeA_dic = treeA.container
            Aset_list= []
            for p in treeA_dic:
                if p != "Root" and p != 'A0':
                    if treeA_dic[p].id == None:
                        A = treeA_dic[p].get_as_string()
                        A_set = list(A)
                        Aset_list.append(A_set)
            matches = 0
#            print nu_tree                                                                              
#            print j                                                                                    
            for B in Bset_list:
                for A in Aset_list:
#                    print B                                                                            
#                    print A                                                                            
                    if sorted(B)==sorted(A):
                        matches +=1
            if matches == len(Bset_list):
#                print nu_tree + '=============================' + j                                    
                same_topology = 'yes'
                break
#    print 'end check topology'
#    print same_topology                                                                                
    return same_topology
