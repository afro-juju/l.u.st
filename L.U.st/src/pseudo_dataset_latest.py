#think this is the latest attempt 8/11/2013
"""
Created by Wasiu Ajenifuja Akanni on 2013.
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.
"""
import random

class pseudo_dataset:
    def get_pseudo_supertrees(self,input_list,replicate_size):
#this method takes in the supertree input tree list and the number of bootstraps wanted and returns a list containing the pseudo dataset list. a list of a list. Needs to be called from within a python script or ipython                                                 
        print 'first start \n'
        input_array = input_list
        replicate_size = replicate_size
        supertree_list = []
        pseudo_input_data = []
        i = 0
        ob= pseudo_dataset()
        while i < replicate_size:
            pseudo_input_trees = ob.get_pseudo_dataset(input_array)
            pseudo_input_data.append(pseudo_input_trees)
            i+=1
#        print 'end of get pseudo supertree \n' + str(len(pseudo_input_data))
#        print pseudo_input_data
        return pseudo_input_data
        

#this script creates a pseudo data set of replicate size for the sake of bootstrapping. Needs to be called from within a python script or ipython with a list of trees as input.                 
 
    def get_pseudo_dataset(self,input_array):
        print 'second start \n'
        input_list = input_array
#        print len(input_list)
        counter = 0
        pseudo_data = []
        while counter < len(input_list):
            input_tree_no = random.randrange(0,len(input_list))
            input_tree = input_list[input_tree_no]
            pseudo_data.append(input_tree)
            counter +=1
        print 'second end \n'
        return pseudo_data
