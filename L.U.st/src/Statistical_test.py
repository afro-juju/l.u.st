#### COMPLETED ON 30/01/2013
"""
Created by Wasiu Ajenifuja Akanni on 2013.
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.
"""

from __future__ import division
import random
import math
import Treecopy2
import Prune
import Sym_diff
import Supertree_likelihood

"""                                                                                                                                             
Created by Wasiu Ajenifuja Akanni on 2013.                                                                                                      
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.                                                                                 
"""
print 'This script along with the Statistical_test.sh and the CONSEL software are you used to perform test such as the AU,SH,KH test for competing hypothesis \n'

def get_like(alpha,beta,distance):

    delta = distance   # un-Normalised tree-to-tree distance.  THis is currently un-Normalised RF but can take any distane (e.g. quartet)
    bd = beta * delta * -1  # remember the -1 might not work AJ.  Test it!  
    exp_bd = math.exp(bd)
    like= (alpha * exp_bd)
    like = math.log10(like)
    return like

supertrees = input('give the name of the file containing your list of supertree: ')
input_trees_file = input('give the name of the file containing your list of input trees: ')
output_file = input('give the name of the file where the input like value of supertrees is to be stored: ')

supertrees_obj = open(supertrees, 'r')
supertrees_list = supertrees_obj.readlines()
print 'there are ' + str(len(supertrees_list)) + ' supertrees'
input_trees_obj = open(input_trees_file, 'r')
input_trees_list= input_trees_obj.readlines()
print 'there are ' + str(len(input_trees_list)) +' input trees'
output_obj = open(output_file , 'a')
output_obj.write('Tree' +"\t"+"-lnL"+ "\t"+"Site" + "\t"+"-lnL" + "\n")

super_counter = 1

comparisons = []

for supertree in supertrees_list:  # loop through supertrees
    
    print ' :-):-):-):-):-):-):-):-)'
    non_normalised_distA = []
    input_likes = []
    count2 = 1
    for input_tree in input_trees_list:
        try:
            input_tree1= Treecopy2.Tree(input_tree, 'I')  # we need here to remake an object to make taxon sets of individual tree (suboptimal but could not think anything better).
        except:
            print "there is an error with the input tree : " + input_tree + str(count2)
        input_dic = input_tree1.container
        no_of_inputtaxa = 0
        inputTree_taxa= []
        for i in input_dic.keys():
            if input_dic[i].id != None:
                no_of_inputtaxa +=1
                inputTree_taxa.append(input_dic[i].id)
        prune_object = Prune.Prune()

#        print 'this is the input tree we sending in ' + input_treeA
#        print no_of_inputtaxa                                                                                                                                                                                                    
#        print 'this is the supertree we are sending in ' + proposed_strA
        pruned_supertree = prune_object.Prune(input_tree,inputTree_taxa,supertree)
#        print ' tis is the out put from prune.py ' + pruned_supertree
        rf_object = Sym_diff.sym_diff()
        distance = rf_object.get_sym_diff(pruned_supertree,input_tree)
        #            normal_dist = (distance)/(((2* no_of_inputtaxa)-6))
        #        print 'this is distance '                                                                       

        alpha = 1
        beta = 1

        like = get_like(alpha,beta,distance)
        like = like-0.1 #We are adding an arbitrary constant to avoid zero likelihood values

        input_likes.append(like)
        count2 +=1
    sum_like = sum(input_likes)*-1
    print 'This is sum of likelihood ' + str(sum_like)
    sum_like = "%.8f" % sum_like #to round off the numbers to 8 decimal places 
    input_counter = 1
    for j in input_likes:
        if j==0.0:
            pass
        else:
            j = j*-1
        j = "%.8f" % j
        output_obj.write("\t"+"\t"+ str(input_counter)+"\t"+str(j) + "\n")
        input_counter +=1
    output_obj.write(str(super_counter) + "\t" +str(sum_like) + "\n")
    super_counter +=1
supertrees_obj.close()
input_trees_obj.close()
output_obj.close()

