"""                                                                                                                                                                                  
Created by Wasiu Ajenifuja Akanni on 2013.                                                                                                                                           
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.                                                                                                                      
"""

import pseudo_dataset1
import ExtractTaxa
import sys
import os.path
from optparse import OptionParser
parser = OptionParser()

parser.add_option("-i", "--input_treefile", action="store", dest="input_file", help="The name of the file containing your list of newick formatted trees: ", metavar="genetreeFILE")
parser.add_option("-r", "--replicates", action="store", type="int", dest="rep_value", help="The number of bootstrap replicates: default = 10 ", default=10)
parser.add_option("-e", "--iterations", action="store", type="int", dest="iteration_no", help=" The number of iterations :  Default= 1 " , default=1)
parser.add_option("-s", "--termination_value", action="store", type="int", dest="termination_no", help=" The stopping or termination number :  Default= 10 " , default=10)
options, args = parser.parse_args()
input_f = options.input_file
if not os.path.exists(input_f):
    print "The input tree file is not a file or does not exist "
    sys.exit(1)

input_trees_file = open(input_f, 'r')
input_trees_list = input_trees_file.readlines()
replicate = options.rep_value

iteration = options.iteration_no
stopping_no = options.termination_no

ob = pseudo_dataset1.pseudo_dataset()
plenary = []
non_plenary = []
pseudo_supertree_list =[]
ml = []
input_taxonset = maketaxon_list.maktaxon_list(input_trees_list)
new_datasets = ob.get_pseudo_supertrees(input_trees_list,replicate)
for i in new_datasets:
    print '##################################################'
    print '##################################################'
    ml = ob.build_supertrees(i,iteration,stopping_no)
    pseudo_supertree_list.append(ml)

print pseudo_supertree_list
for p in pseudo_supertree_list:
    print p
    p_taxonset = maketaxon_list.maktaxon_list(p)
    if len(p_taxonset) == len(input_taxonset):
        plenary.append(p)
    else:
        non_plenary.append(p)

print len(pseudo_supertree_list)
print '##########################################'
print pseudo_supertree_list
 

print '###################################'
print 'plenary supertrees'
print '######################################'
print plenary

print '###################################'
print 'non plenary supertrees'
print '######################################'
print non_plenary

print "finito!!!"
