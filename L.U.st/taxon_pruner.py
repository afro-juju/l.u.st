"""
Created by Wasiu Ajenifuja Akanni on 2013.
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.
"""

print "This script take a list of trees and a list of taxa that are to be pruned from the tree and returns the pruned trees"
import Treecopy2
import sys
import os.path
sys.path.append('src')
from optparse import OptionParser
parser = OptionParser()

parser.add_option("-i", "--input_treefile", action="store", dest="input_file", help="The name of the file containing your list of newick formatted trees: ", metavar="genetreeFILE")
parser.add_option("-r", "--taxa_file", action="store", dest="taxa_file", help="The name of the file containing the taxon list ", metavar="taxaFILE")
parser.add_option("-o", "--output_file", action="store", dest="outfile", help=" The name of the file the results are to be printed into :  Default= Taxon_pruner_output " , default= "Taxon_pruner_output")

options, args = parser.parse_args()
input_f = options.input_file
if not os.path.exists(input_f):
    print "The input tree file is not a file or does not exist "
    sys.exit(1)
trees_obj =open(input_f, 'r')
trees = trees_obj.readlines()

input_taxa = options.taxa_file
if not os.path.isfile(input_taxa):
    print "The input taxa file is not a file or does not exist "
    sys.exit(1)
taxa_obj = open(input_taxa,'r')
taxa = taxa_obj.readlines()
outfile = options.outfile
outp= open(outfile,'a')
nu_trees = []
for tree in trees:
#    print tree
    working_tree = tree  
    for tax in taxa:  
        working_cp = Treecopy2.Tree(working_tree, 'A')
        working_dic = working_cp.container
        for i in working_dic.keys():
            if working_dic[i].id == tax.strip():
                working_tree = working_dic['Root'].get_chopped(i)
    nu_trees.append(working_tree.replace(' ',''))
    outp.write(working_tree.replace(' ','') + ";\n")
#    print working_tree.replace(' ','')
print len(nu_trees)    
trees_obj.close()
taxa_obj.close()
outp.close()
print "FINISHED!!!! \n"
