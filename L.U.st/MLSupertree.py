"""
Created by Wasiu Ajenifuja Akanni on 2013.
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.
"""


from time import time
import sys
import os.path
sys.path.append('src')
import Stepwise
import fisher_yates
import Treecopy2
import check_topology
import ExtractTaxa
import Supertree_likelihood
class BreakOutOfForLoop5(Exception):pass
class BreakOutOfForLoop4(Exception):pass
class BreakOutOfForLoop3(Exception):pass
class BreakOutOfForLoop2(Exception):pass
class BreakOutOfForLoop(Exception):pass
from optparse import OptionParser
parser = OptionParser()
parser.add_option("-i", "--input_treefile", action="store", dest="input_file", help="the name of the file containing the fully resolved input gene trees in newick format", metavar="genetreeFILE")
parser.add_option("-t", "--start_tree_option", action="store", dest="startin_option", help=" 'yes': if you have a starting tree. Default = 'no' ", default='no')
parser.add_option("-s", "--start_treefile", action="store", dest="start_tree_file", help=" the name of the file containing the fully resolved starting supertree(s) in newick format ")
parser.add_option("-n", "--number_of_iterations", action="store", type="int", dest="random_iterations", help=" the number of random starting trees/iterations. Deafault=1 ", default=1)
parser.add_option("-o", "--output_file", action="store", dest="outfile", help=" the name of the file where you want the output of the analysis to be stored. Default= Mlsupertree_analysis_output " , default= "Mlsupertree_analysis_output")
parser.add_option("-c", "--heuristic", action="store", type="int", dest="spr_choice", help= "please type 1: for a full exhaustive spr search \n 2: for a version that does not go thru every rooting point \n 3: a version that only considereds better trees \n 4: a version that does not include going through all the roots and only considers trees with better likelihood. Default=1 " , default=1)

options, args = parser.parse_args()

input_file=options.input_file #maybe i could put an error messages here if the user has failed to provide an approriate input file name
if not os.path.isfile(input_file):
    print "Error: The input file given isn't a file or does not exit "
    sys.exit(1)
input_tree_handler = open(input_file, 'r')
input_trees = input_tree_handler.readlines() #A list of all the input trees
input_tree_handler.close()
input_taxa = ExtractTaxa.maktaxon_list(input_trees) #creates a list of all the taxa from the set of input trees.
count = 0 
overall_best = [] #will contain all the best trees we find. trees of equal value.
stepwise_ob = Stepwise.stepwise()
overall_like = None
root = input_taxa[0] # this is the arbitrary root that all the trees will be rooted on
stepwise_tree_list = []
startin_option = options.startin_option #maybe i could put a error message here that checks to make sure the user has put only yes or no as input here 
if startin_option == 'yes':
    count = 0
    random_iterations = options.random_iterations
    start_tree_file =options.start_tree_file #maybe i could put an error messages here if the user has failed to provide an approriate start supertree file name
    if os.path.isfile(start_tree_file):
        print "Error: The start_tree_file given isn't a file or does not exit "
        sys.exit(1)
    start_tree_handler = open(start_tree_file, 'r')
    tree = start_tree_handler.read()
    while count != random_iterations:
        stepwise_tree_list.append(tree)
        count +=1
    start_tree_handler.close()
else:
    random_iterations = options.random_iterations
    count = 0
    while count != random_iterations:
        random_tree = stepwise_ob.get_stepwise_tree(input_taxa)
        if stepwise_tree_list == [] :
            stepwise_tree_list.append(random_tree)
            count +=1
        else:
#            print count 
#            print random_iterations
#            print random_tree
#            print stepwise_tree_list
#            print 'yabadabadab'
            check = check_topology.is_newtopology(random_tree,stepwise_tree_list) # i could make this quicker my checking first if stepwise_tree_list is empty as that would me i wont have to do this step.
            if check != 'yes':
                stepwise_tree_list.append(random_tree)
                count +=1

outfile = options.outfile
outf = open(outfile, 'w')

spr_choice = options.spr_choice
trees_2_swap = {}
trees_seen = []

#next we harvest the taxon set for each input tree and set it as the value with the input tree as the key in a dictionary. we do this to speed up the calculations, so we wont have to do it every time we need to prune the supertree to have the same taxon set as the input tree
input_treeTaxonSet = {}
for tree in input_trees:
#    print " this is the input tree " , tree                                                                                                                                         
    tree_ob = Treecopy2.Tree(tree, 'T')
    tree_dic = tree_ob.container
    tree_taxonSet=[]
#the for loop below gives me a list of all the possible rooting position for the current tree                                                                                        
    root_pt = ''
    for key in tree_dic.keys():
        if tree_dic[key].id != None:
            if tree_taxonSet == []:
                root_pt = key
            tree_taxonSet.append(tree_dic[key].id)

#    tree_taxonSet.sort()                                                                                                                                                            
    tree_rerooted = tree_dic[root_pt].Reroot()
#    print "this is the current tree, current tree rerooted and taxon set ",tree, "  ",tree_rerooted," ",  tree_taxonSet                                                             

    input_treeTaxonSet[tree_rerooted] = tree_taxonSet


t0 = time()
if spr_choice == 5:

    DidIJump = 'no' # checker flag... if we jumped cuz we found a better tree it is set to yes, if its cuz we have search all the rootin of a tree and nw want to go tru the trees to swap it set to mayb, if we have checked all tree and all rooting and we want to pick a new stpwise random tree it is set to no.                                                     
    current_tree = None
    current_tree_like = None
    bigcount = 0 # the very outer counter for the very outer while loop                                                     
    new_tree_like = None
    lagos = '' # will be used to store the new tree we get from the tree to swap list                                       
    while bigcount <len(stepwise_tree_list):
#        print 'BACK AGAIN IN THE BIGCOUNT WHILE LOOOP '+ str(bigcount)
#        print ' This are the trees in overall' + str(len(overall_best)) + '\n'
#        print overall_best
        if DidIJump =='yes':
#            print 'DIDIJUMP ==================================YES \n'
            current_tree = current_tree
            if overall_like == None or new_tree_like > overall_like:
                overall_like = new_tree_like
                overall_best =[]
                overall_best.append(current_tree)
            elif overall_like ==  new_tree_like:
                check = check_topology.is_newtopology(current_tree,overall_best)
                if check != 'yes':
                    overall_best.append(current_tree)
                    # mayb i should close both of the if loops above off with an else statement.....will look into this later
        elif DidIJump == 'mayb':
#            print 'DIDIJUMP ==================================MAYBE\n'
            current_tree = lagos

        else:
            print 'Now in Iteration: ' + str(bigcount)
            next_tree = stepwise_tree_list[bigcount]
            current_tree = next_tree
        DidIJump = 'no'
#        print '#####################this is the current tree################ ' + current_tree + "\n"
        current_tree_ob = Treecopy2.Tree(current_tree, 'c')
        current_tree_dic = current_tree_ob.container
        current_root_pts=[]
#the for loop below gives me a list of all the possible rooting position for the current tree
        for key in current_tree_dic.keys():
            if key != 'c0' and key != 'Root':
                current_root_pts.append(key)
        root_pt = ''
#        print 'these is the number of rooting points' + str(len(current_root_pts)) + "\n"
#the for loop below goes through each of the possible rooting pts and reroots the current tree at a different one each time
        try:
            for pt in current_root_pts:
#                print ' This are the trees in overall \n'
#                print overall_best
#                print 'COMING BACK FOR A NEWWww ROOTTTTTTTT'
#                print '#########################STATS UPDATES22222222########################'
#                print 'number of trees in trees left 2 swap: ' + str(len(trees_2_swap.keys()))
#                print 'number random trees that we are on: ' + str(bigcount)
#                print 'number of trees in best overall: ' + str(len(overall_best)) + ' ' + str(overall_like)
                root_pt = pt #this is the new rerooting pt of the current tree
                current_rerooted = current_tree_dic[root_pt].Reroot()

                current_subtrees_lst = []
                current_subtrees_dic = {}
                
                current_rerooted_ob = Treecopy2.Tree(current_rerooted, 'r')
                current_rerooted_dic = current_rerooted_ob.container
                        
                current_rerooting_pt = None # this will hold the node identifier of the arbitrary root... save time havin to build the tree again when i want to reroot it .
#the for loop below gets all the possible subtrees of the newly rerooted tree and it also identifies the key for the arbitrary rooting position and also creats a dictionary with the subtrees as keys and there corresponding keys as the value...this will be used later on when cutting the subtree from current tree
                for key in current_rerooted_dic.keys():
                    if key != 'r0' and key != 'Root':
                        s = current_rerooted_dic[key].get_as_string()
                        if current_rerooted_dic[key].id == root:
                            current_rerooting_pt = key 
                        if s not in current_subtrees_lst:
                            current_subtrees_lst.append(s)
                            current_subtrees_dic[s] =key
                current_subtrees_lst = fisher_yates.shuffle(current_subtrees_lst)


                like_ob = Supertree_likelihood.Supertree_likelihood()
                current_tree_like = like_ob.get_superlik(input_treeTaxonSet,current_rerooted)
                new_tree_like = 0
                count1 = 0

                while count1 < len(current_subtrees_lst):
#                    print 'COUNT1 WHILE AGAINNNNNNNNNNNN' +str(count1)
                    count2 = count1 + 1
                    
                    while count2 < len(current_subtrees_lst):
#                        print 'COUNT2 WHILE AGAINNNNNNNNNNNN' +str(count2)
                        a=(current_subtrees_lst[count1]) # this is like this cuz i needed a list                                                                          
                        b=(current_subtrees_lst[count2]) # same as above                                                                                                  
                        #                    print a                                                                                                                                          
                        #                    print b                                                                                                                                          
                        a = a.replace('(','')
                        a = a.replace(')','')
                        a = a.replace(',',' ')
                        a = a.split(' ')
                        a1 = []
                        for i in a:
                            if i =='':
                                pass
                            else:
                                a1.append(i)
                        a = a1

                        b =b.replace('(','')
                        b = b.replace(')','')
                        b =b.replace(',',' ')
                        b =b.split(' ')
                        b1 = []
                        for j in b:
                            if j =='':
                                pass
                            else:
                                b1.append(j)
                        b = b1
                        new_tree_str='' # i dont no y this is here                                                                                                        
                        if set(a).intersection(set(b)):
#                            print 'BADDDDDDDDDDDDDDDDDD SWAP'
                            count2 +=1
                            #                        print a
                            #                        print b  
                            continue

                        else:
                            #                        print a
                            #                        print b   
                            prun_pt = current_subtrees_dic[current_subtrees_lst[count1]]
                            #                        print prun_pt             
                            cut_out = current_rerooted_dic[prun_pt].get_as_string()
                            base = current_rerooted_dic['Root'].get_chopped(prun_pt)
                            #                        print 'this is the treee ' + current_rerooted
                            #                        print 'this is cut out ' + cut_out
                            #                        print 'this is the base ' + base                                                                                                             
                            base_sub_dic = {}
#this next if loop gets the subtrees of the base and stores them as keys in the dictionary labelled base_sub_dic with their corresponding keys as the values. this will help us when to identify the key in the new base tree that correspond to the 2nd subtree from the original tree so that we can cut/prun it out. 
                            if '(' in base and base != current_subtrees_lst[count2]:
                                t4 = Treecopy2.Tree(base,'n')
                                t1_dic = t4.container
                                for key in t1_dic.keys():
                                    if key != 'n0' and key != 'Root':
                                        s = t1_dic[key].get_as_string()
                                        if s not in base_sub_dic.keys():
                                            base_sub_dic[s] =key

                                in_pt = current_subtrees_lst[count2] #this contains the 2nd subtree from the current tree that we want to join to the first subtree labelled cut_out 
                                #                            print 'this is base subtrees '
                                #                            print base_sub_dic.keys()
                                join_pt = base_sub_dic[in_pt] #this now gives us the new key position of the 2nd subtree in the base tree
                                insert_str = t1_dic[join_pt].get_as_string() # we use this prunned out the 2nd subtree from the base tree
                                rest = t1_dic['Root'].get_chopped(join_pt) # we use this to get everything else that remains from the current rerooted tre
                                new_tree_str = '((' + insert_str + ','+ cut_out + ')'+','+ rest + ')' # nw we join everythin back together in their new places to form a new spr tree
#                                print 'this is the new tree : ' + new_tree_str

                                new_tree_like = like_ob.get_superlik(input_treeTaxonSet,new_tree_str) 

                                #                            print current_tree_like   
                                #                            print new_tree_like 
                                new_tree_str1 = None
                                new_tree_obj = Treecopy2.Tree(new_tree_str, 'w')
                                new_tree_dic=new_tree_obj.container
#this next for loop is to reroot the new tree to the original root so that all the trees we are comparing have the same root
                                for i in new_tree_dic.keys():
                                    if i != 'Root' and i != 'w0':
                                        if new_tree_dic[i].id == root:
                                            new_tree_str1 = new_tree_dic[i].Reroot()


#this next set of if loops compares the likelihood of the new tree from the spr process to the current_tree rerooted
                                if float(new_tree_like) > float(current_tree_like):
#                                    print' FOUND A NEW BETTERRRRRRRRRRRRRRRR TREE YAAAYYYYY'
                                    current_tree = new_tree_str1
                                    DidIJump = 'yes'
                                    raise BreakOutOfForLoop

                                elif float(new_tree_like) == float(current_tree_like):
#                                    print 'SAME LIKEEEEEEEEEEEEEEEEEEEE'
#                                    print 'NEW TREE IN THE SWAP LISTTTTTTTTTTTTT'
                                    if new_tree_str1 not in trees_seen:
                                        check = check_topology.is_newtopology(new_tree_str1,trees_seen)
                                        if check != 'yes':
                                            if new_tree_str1 not in trees_2_swap:
                                                check = check_topology.is_newtopology(new_tree_str1,trees_2_swap)
                                                if check !='yes':
                                                    if new_tree_str1 not in overall_best:
                                                        check = check_topology.is_newtopology(new_tree_str1,overall_best)
                                                        if check !='yes':
                                                            trees_2_swap[new_tree_str1] = new_tree_like
#                                                            print 'SAME LIKEEEEEEEEEEEEEEEEEEEE' + str(len(trees_2_swap.keys())) + '\n'
                                    count2+=1
                                    continue
                                elif float(new_tree_like) < float(current_tree_like):
#                                    print 'shitttttttt treeeeeeeeeee'
                                    count2+=1
                                    continue
                            else:
                                #                            print 'BADDDDDDDDDDDDDDDDDD SWAP AGAINNNNNNNNNNNNNNNN'
                                count2 +=1
                                continue
#                        print 'GUESSSSSSSS WHO FINISHED COUNT2 WHILEEEEEEEEEEEEEE'
                        count2 +=1
#                    print 'GUESSSSSSSS WHO FINISHED COUNT1 WHILEEEEEEEEEEEEEE'
                    count1 +=1
                current_rerooted1 = current_rerooted_dic[current_rerooting_pt].Reroot()
#we get here if we have gone through every possible spr for the rerooted current tree
# we compare the current rerooted to the overall best tree found so far 
                if overall_like == None or current_tree_like > overall_like:
                    overall_like = new_tree_like
                    overall_best =[]
                    overall_best.append(current_rerooted1)
                elif overall_like ==  current_tree_like:
                    check = check_topology.is_newtopology(current_rerooted1,overall_best)
                    if check != 'yes':
                        overall_best.append(current_rerooted1)

 
        except BreakOutOfForLoop: # we get here after we find a better tree through our spr... from here we go back to start, this using the new tree as our current tree.
#
#            print 'we just employed the break, nw were are going '
            continue
        if len(trees_2_swap.keys())!=0:
            print 'Now we are IN THE SWAP LIST. This is the number of trees in the swap list:  '+ str(len(trees_2_swap.keys()))
            nu_tuple = trees_2_swap.popitem()
            lagos = nu_tuple[0]
            trees_seen.append(lagos)
            DidIJump= 'mayb'
            continue
#        print 'GUESSSSSSSS WHO FINISHED BIGCOUNT WHILEEEEEEEEEEEEEE'
        print overall_best
        bigcount +=1


elif spr_choice == 2:
        
    DidIJump = 'no' # checker flag... if we jumped cuz we found a better tree it is set to yes, if its cuz we have search all the rootin of a tree and nw want to go tru the trees to swap it set to mayb, if we have checked all tree and all rooting and we want to pick a new stpwise random tree it is set to no.                  
    current_tree = None
    current_tree_like = None
    bigcount = 0 # the very outer counter for the very outer while loop                                                     
    new_tree_like = None
    lagos = '' # will be used to store the new tree we get from the tree to swap list                                       
    while bigcount <len(stepwise_tree_list):
#        print 'BACK AGAIN IN THE BIGCOUNT WHILE LOOOP '+ str(bigcount)
#        print ' This are the trees in overall' + str(len(overall_best)) + '\n'
#        print overall_best        
        if DidIJump =='yes':
#            print 'DIDIJUMP ==================================YES \n'
            current_tree = current_tree
            if overall_like == None or new_tree_like > overall_like:
                overall_like = new_tree_like
                overall_best =[]
                overall_best.append(current_tree)
            elif overall_like ==  new_tree_like:
                check = check_topology.is_newtopology(current_tree,overall_best)
                if check != 'yes':
                    overall_best.append(current_tree)
            trees_2_swap = {}
            trees_seen = []

        elif DidIJump == 'mayb':
#            print 'DIDIJUMP ==================================MAYBE \n'
            current_tree = lagos

        else:
            print 'Now in iteration: ' + str(bigcount)
            next_tree = stepwise_tree_list[bigcount]
            current_tree = next_tree
            trees_2_swap = {}
            trees_seen = []
        DidIJump = 'no'
        root_pt = ''

#        print ' THIS IS THE CURRENT TREE ' + current_tree 
#        print ' This are the trees in overall ' 
#        print overall_best
#        print 'COMING BACK into tryyyyyyyyyyyyyyyyyyyy'
#        print '#########################STATS UPDATES333333333########################'
#        print 'number of trees in trees left 2 swap: ' + str(len(trees_2_swap.keys()))
#        print 'number random trees that we are on: ' + str(bigcount)
#        print 'number of trees in best overall: ' + str(len(overall_best)) + str(overall_like)
        
        current_subtrees_lst = []
        current_subtrees_dic = {}
        
        current_tree_ob = Treecopy2.Tree(current_tree, 'r')
        current_tree_dic = current_tree_ob.container
        
        current_rerooting_pt = None # this will hold the node identifier of the root... save time havin to build the tree again when i want to reroot it .
        for key in current_tree_dic.keys():
            if key != 'r0' and key != 'Root':
                s = current_tree_dic[key].get_as_string()
                if current_tree_dic[key].id == root:
                    current_rerooting_pt = key 
                if s not in current_subtrees_lst:
                    current_subtrees_lst.append(s)
                    current_subtrees_dic[s] =key
        current_subtrees_lst = fisher_yates.shuffle(current_subtrees_lst)

        like_ob = Supertree_likelihood.Supertree_likelihood()
        current_tree_like = like_ob.get_superlik(input_treeTaxonSet,current_tree)
        new_tree_like = 0
        count1 = 0
        try:
            while count1 < len(current_subtrees_lst):
#                print 'COUNT1 WHILE AGAINNNNNNNNNNNN' +str(count1)
                count2 = count1+1
                
                while count2 < len(current_subtrees_lst):
#                    print 'COUNT2 WHILE AGAINNNNNNNNNNNN' +str(count2)
                    a=(current_subtrees_lst[count1]) # this is like this cuz i needed a list                                                                          
                    b=(current_subtrees_lst[count2]) # same as above                                                                                                  
                    #                    print a                                                                                                                                          
                    #                    print b                                                                                                                                          
                    a = a.replace('(','')
                    a = a.replace(')','')
                    a = a.replace(',',' ')
                    a = a.split(' ')
                    a1 = []
                    for i in a:
                        if i =='':
                            pass
                        else:
                            a1.append(i)
                    a = a1

                    b =b.replace('(','')
                    b = b.replace(')','')
                    b =b.replace(',',' ')
                    b =b.split(' ')
                    b1 = []
                    for j in b:
                        if j =='':
                            pass
                        else:
                            b1.append(j)
                    b = b1
                    new_tree_str='' # i dont no y this is here                                                                                                        
                    if set(a).intersection(set(b)):
#                        print 'BADDDDDDDDDDDDDDDDDD SWAP'
                        count2 +=1
                        #                        print a
                        #                        print b  
                        continue
    
                    else:
                        #                        print a
                        #                        print b   
                        prun_pt = current_subtrees_dic[current_subtrees_lst[count1]]
                        #                        print prun_pt             
                        cut_out = current_tree_dic[prun_pt].get_as_string()
                        base = current_tree_dic['Root'].get_chopped(prun_pt)
                        #                        print 'this is the treee ' + current_rerooted
                        #                        print 'this is cut out ' + cut_out
                        #                        print 'this is the base ' + base                                                                                                             
                        base_sub_dic = {}
                        if '(' in base and base != current_subtrees_lst[count2]:
                            t4 = Treecopy2.Tree(base,'n')
                            t1_dic = t4.container
                            for key in t1_dic.keys():
                                if key != 'n0' and key != 'Root':
                                    s = t1_dic[key].get_as_string()
                                    if s not in base_sub_dic.keys():
                                        base_sub_dic[s] =key

                            in_pt = current_subtrees_lst[count2]
                            #                            print 'this is base subtrees '
                            #                            print base_sub_dic.keys()
                            join_pt = base_sub_dic[in_pt]
                            insert_str = t1_dic[join_pt].get_as_string()
                            rest = t1_dic['Root'].get_chopped(join_pt)
                            new_tree_str = '((' + insert_str + ','+ cut_out + ')'+','+ rest + ')'

                            new_tree_like = like_ob.get_superlik(input_treeTaxonSet,new_tree_str)
                            #                            print current_tree_like   
                            #                            print new_tree_like 

#this next for loop is to reroot the new tree to the original root so that all the trees we are comparing have the same root
                            new_tree_str1 = None
                            new_tree_obj = Treecopy2.Tree(new_tree_str, 'w')
                            new_tree_dic=new_tree_obj.container
                            for i in new_tree_dic.keys():
                                if i != 'Root' and i != 'w0':
                                    if new_tree_dic[i].id == root:
                                        new_tree_str1 = new_tree_dic[i].Reroot()

                                
                            if float(new_tree_like) > float(current_tree_like):
#                                print' FOUND A NEW BETTERRRRRRRRRRRRRRRR TREE YAAAYYYYY'
                                current_tree = new_tree_str1
                                DidIJump = 'yes'
                                raise BreakOutOfForLoop2

                            elif float(new_tree_like) == float(current_tree_like):
#                                print 'SAME LIKEEEEEEEEEEEEEEEEEEEE'
                                #                                    print 'NEW TREE IN THE SWAP LISTTTTTTTTTTTTT'
                                
                                check = check_topology.is_newtopology(new_tree_str1,trees_seen)
                                if check != 'yes':
                                    check = check_topology.is_newtopology(new_tree_str1,trees_2_swap)
                                    if check !='yes':
                                        check = check_topology.is_newtopology(new_tree_str1,overall_best)
                                        if check !='yes':
                                            trees_2_swap[new_tree_str1] = new_tree_like
#                                            print 'SAME LIKEEEEEEEEEEEEEEEEEEEE' + str(len(trees_2_swap.keys()))
                                count2+=1
                                continue
                            else:
                                count2+=1
                                continue
                        else:
                            #                            print 'BADDDDDDDDDDDDDDDDDD SWAP AGAINNNNNNNNNNNNNNNN'
                            count2 +=1
                            continue
#                    print 'GUESSSSSSSS WHO FINISHED COUNT2 WHILEEEEEEEEEEEEEE'   
                    count2 +=1
#                print 'GUESSSSSSSS WHO FINISHED COUNT1 WHILEEEEEEEEEEEEEE'
                count1 +=1
            current_rerooted1 = current_tree_dic[current_rerooting_pt].Reroot()
            if overall_like == None or current_tree_like > overall_like:
                overall_like = new_tree_like
                overall_best =[]
                overall_best.append(current_rerooted1)
            elif overall_like ==  current_tree_like:
                check = check_topology.is_newtopology(current_rerooted1,overall_best)
                if check != 'yes':
                    overall_best.append(current_rerooted1)

 
        except BreakOutOfForLoop2:
            continue
        if len(trees_2_swap.keys())!=0:
            print 'Now in the SWAP LIST. This is the number of trees in Swap list: ' + str(len(trees_2_swap.keys()))
            nu_tuple = trees_2_swap.popitem()
            lagos = nu_tuple[0]
            trees_seen.append(lagos)
            DidIJump= 'mayb'
            continue
        #        print 'GUESSSSSSSS WHO FINISHED BIGCOUNT WHILEEEEEEEEEEEEEE'
        print overall_best
        bigcount +=1

if spr_choice == 3:
        
    DidIJump = 'no' # checker flag... if we jumped cuz we found a better tree it is set to yes, if its cuz we have search all the rootin of a tree and nw want to go tru the trees to swap it set to mayb, if we have checked all tree and all rooting and we want to pick a new stpwise random tree it is set to no.                                                     
    current_tree = None
    current_tree_like = None
    bigcount = 0 # the very outer counter for the very outer while loop                                                     
    new_tree_like = None
    lagos = '' # will be used to store the new tree we get from the tree to swap list                                       
    while bigcount <len(stepwise_tree_list):
#        print 'BACK AGAIN IN THE BIGCOUNT WHILE LOOOP '+ str(bigcount)
#        print ' This are the trees in overall' + str(len(overall_best)) + '\n'
#        print overall_best
        if DidIJump =='yes':
#            print 'DIDIJUMP ==================================YES \n'
            current_tree = current_tree
            if overall_like == None or new_tree_like > overall_like:
                overall_like = new_tree_like
                overall_best =[]
                overall_best.append(current_tree)
            elif overall_like ==  new_tree_like:
                check = check_topology.is_newtopology(current_tree,overall_best)
                if check != 'yes':
                    overall_best.append(current_tree)
        else:
            print 'Now in Iteration: ' + str(bigcount)
#            print 'DIDIJUMP ==================================NO \n'
            next_tree = stepwise_tree_list[bigcount]
            current_tree = next_tree
        DidIJump = 'no'
        
        current_tree_ob = Treecopy2.Tree(current_tree, 'c')
        current_tree_dic = current_tree_ob.container
        current_root_pts=[]
        for key in current_tree_dic.keys():
            if key != 'c0' and key != 'Root':
                current_root_pts.append(key)
        root_pt = ''
#        print ' THIS IS THE CURRENT TREE ' + current_tree
#        print 'these is the number of rooting points' + str(len(current_root_pts))
        try:
            for pt in current_root_pts:
#                print ' This are the trees in overall '
#                print overall_best
#                print 'COMING BACK FOR A NEWW ROOTTTTTTTT'
#                print '#########################STATS UPDATES4444########################'
#                print 'number random trees that we are on: ' + str(bigcount)
#                print 'number of trees in best overall: ' + str(len(overall_best)) + str(overall_like)
                root_pt = pt
                current_rerooted = current_tree_dic[root_pt].Reroot()

                current_subtrees_lst = []
                current_subtrees_dic = {}
                
                current_rerooted_ob = Treecopy2.Tree(current_rerooted, 'r')
                current_rerooted_dic = current_rerooted_ob.container
                        
                current_rerooting_pt = None # this will hold the node identifier of the root... save time havin to build the tree again when i want to reroot it .
                for key in current_rerooted_dic.keys():
                    if key != 'r0' and key != 'Root':
                        s = current_rerooted_dic[key].get_as_string()
                        if current_rerooted_dic[key].id == root:
                            current_rerooting_pt = key 
                        if s not in current_subtrees_lst:
                            current_subtrees_lst.append(s)
                            current_subtrees_dic[s] =key
                current_subtrees_lst = fisher_yates.shuffle(current_subtrees_lst)


                like_ob = Supertree_likelihood.Supertree_likelihood()
                current_tree_like = like_ob.get_superlik(input_treeTaxonSet,current_rerooted)
                new_tree_like = 0
                count1 = 0

                while count1 < len(current_subtrees_lst):
#                    print 'COUNT1 WHILE AGAINNNNNNNNNNNN' +str(count1)
                    count2 = count1+1
                    
                    while count2 < len(current_subtrees_lst):
#                        print 'COUNT2 WHILE AGAINNNNNNNNNNNN' +str(count2)
                        a=(current_subtrees_lst[count1]) # this is like this cuz i needed a list                                                                          
                        b=(current_subtrees_lst[count2]) # same as above                                                                                                  
                        #                    print a                                                                                                                                          
                        #                    print b                                                                                                                                          
                        a = a.replace('(','')
                        a = a.replace(')','')
                        a = a.replace(',',' ')
                        a = a.split(' ')
                        a1 = []
                        for i in a:
                            if i =='':
                                pass
                            else:
                                a1.append(i)
                        a = a1

                        b =b.replace('(','')
                        b = b.replace(')','')
                        b =b.replace(',',' ')
                        b =b.split(' ')
                        b1 = []
                        for j in b:
                            if j =='':
                                pass
                            else:
                                b1.append(j)
                        b = b1
                        new_tree_str='' # i dont no y this is here                                                                                                        
                        if set(a).intersection(set(b)):
                            #                        print 'BADDDDDDDDDDDDDDDDDD SWAP'
                            count2 +=1
                            #                        print a
                            #                        print b  
                            continue

                        else:
                            #                        print a
                            #                        print b   
                            prun_pt = current_subtrees_dic[current_subtrees_lst[count1]]
                            #                        print prun_pt             
                            cut_out = current_rerooted_dic[prun_pt].get_as_string()
                            base = current_rerooted_dic['Root'].get_chopped(prun_pt)
                            #                        print 'this is the treee ' + current_rerooted
                            #                        print 'this is cut out ' + cut_out
                            #                        print 'this is the base ' + base                                                                                                             
                            base_sub_dic = {}
                            if '(' in base and base != current_subtrees_lst[count2]:
                                t4 = Treecopy2.Tree(base,'n')
                                t1_dic = t4.container
                                for key in t1_dic.keys():
                                    if key != 'n0' and key != 'Root':
                                        s = t1_dic[key].get_as_string()
                                        if s not in base_sub_dic.keys():
                                            base_sub_dic[s] =key

                                in_pt = current_subtrees_lst[count2]
                                #                            print 'this is base subtrees '
                                #                            print base_sub_dic.keys()
                                join_pt = base_sub_dic[in_pt]
                                insert_str = t1_dic[join_pt].get_as_string()
                                rest = t1_dic['Root'].get_chopped(join_pt)
                                new_tree_str = '((' + insert_str + ','+ cut_out + ')'+','+ rest + ')'

                                new_tree_like = like_ob.get_superlik(input_treeTaxonSet,new_tree_str)
                                #                            print current_tree_like   
                                #                            print new_tree_like 

#this next for loop is to reroot the new tree to the original root so that all the trees we are comparing have the same root
                                new_tree_str1 = None
                                new_tree_obj = Treecopy2.Tree(new_tree_str, 'w')
                                new_tree_dic=new_tree_obj.container
                                for i in new_tree_dic.keys():
                                    if i != 'Root' and i != 'w0':
                                        if new_tree_dic[i].id == root:
                                            new_tree_str1 = new_tree_dic[i].Reroot()

                                
                                if float(new_tree_like) > float(current_tree_like):
#                                    print' FOUND A NEW BETTERRRRRRRRRRRRRRRR TREE YAAAYYYYY'
                                    current_tree = new_tree_str1
                                    DidIJump = 'yes'
                                    raise BreakOutOfForLoop3

                                elif float(new_tree_like) < float(current_tree_like):
                                    count2+=1
                                    continue
                            else:
                                #                            print 'BADDDDDDDDDDDDDDDDDD SWAP AGAINNNNNNNNNNNNNNNN'
                                count2 +=1
                                continue
#                        print 'GUESSSSSSSS WHO FINISHED COUNT2 WHILEEEEEEEEEEEEEE'
                        count2 +=1
#                    print 'GUESSSSSSSS WHO FINISHED COUNT1 WHILEEEEEEEEEEEEEE'
                    count1 +=1
                current_rerooted1 = current_rerooted_dic[current_rerooting_pt].Reroot()
                if overall_like == None or current_tree_like > overall_like:
                    overall_like = new_tree_like
                    overall_best =[]
                    overall_best.append(current_rerooted1)
                elif overall_like ==  current_tree_like:
                    check = check_topology.is_newtopology(current_rerooted1,overall_best)
                    if check != 'yes':
                        overall_best.append(current_rerooted1)

 
        except BreakOutOfForLoop3:
            continue
#        print 'GUESSSSSSSS WHO FINISHED BIGCOUNT WHILEEEEEEEEEEEEEE'
        print overall_best
        bigcount +=1



elif spr_choice == 4:

    DidIJump = 'no' # checker flag... if we jumped cuz we found a better tree it is set to yes, if its cuz we have search all the rootin of a tree and  want to go tru the trees to swap it set to mayb, if we have checked all tree and all rooting and we want to pick a new stpwise random tree it is set to no.                                                     
    current_tree = None
    current_tree_like = None
    bigcount = 0 # the very outer counter for the very outer while loop                                                     
    new_tree_like = None
    lagos = '' # will be used to store the new tree we get from the tree to swap list                                       
    while bigcount <len(stepwise_tree_list):
#        print 'BACK AGAIN IN THE BIGCOUNT WHILE LOOOP '+ str(bigcount)
#        print ' This are the trees in overall' + str(len(overall_best)) + '\n'
#        print overall_best
        if DidIJump =='yes':
#            print 'DIDIJUMP ==================================YES \n'
            current_tree = current_tree
            if overall_like == None or new_tree_like > overall_like:
                overall_like = new_tree_like
                overall_best =[]
                overall_best.append(current_tree)
            elif overall_like ==  new_tree_like:
                check = check_topology.is_newtopology(current_tree,overall_best)
                if check != 'yes':
                    overall_best.append(current_tree)
        else:
            print 'Now in Iteration: ' + str(bigcount)
            next_tree = stepwise_tree_list[bigcount]
            current_tree = next_tree
        DidIJump = 'no'
        root_pt = ''
#        print 'THIS IS THE CURRENT TREE#####################' + current_tree
        try:
#            print 'COMING BACK into tryyyyyyyyyyyyyyyyyyyy'
#            print '#########################STATS UPDATES5555########################'
#            print 'number random trees that we are on: ' + str(bigcount)
#            print 'number of trees in best overall: ' + str(len(overall_best)) + str(overall_like)
#            print ' This are the trees in overall '
#            print overall_best
            current_subtrees_lst = []
            current_subtrees_dic = {}
            
            current_tree_ob = Treecopy2.Tree(current_tree, 'r')
            current_tree_dic = current_tree_ob.container
                        
            current_rerooting_pt = None # this will hold the node identifier of the root... save time havin to build the tree again when i want to reroot it .
            for key in current_tree_dic.keys():
                if key != 'r0' and key != 'Root':
                    s = current_tree_dic[key].get_as_string()
                    if current_tree_dic[key].id == root:
                        current_rerooting_pt = key 
                    if s not in current_subtrees_lst:
                        current_subtrees_lst.append(s)
                        current_subtrees_dic[s] =key
            current_subtrees_lst = fisher_yates.shuffle(current_subtrees_lst)

            like_ob = Supertree_likelihood.Supertree_likelihood()
            current_tree_like = like_ob.get_superlik(input_treeTaxonSet,current_tree)
            new_tree_like = 0
            count1 = 0
#            print 'THIS IS THE NUMBER OF SUBTREES ' + str(len(current_subtrees_lst))
            while count1 < len(current_subtrees_lst):
#                print 'COUNT1 WHILE AGAINNNNNNNNNNNN' +str(count1)
                count2 = count1+1
                
                while count2 < len(current_subtrees_lst):
#                    print 'COUNT2 WHILE AGAINNNNNNNNNNNN' +str(count2)
                    a=(current_subtrees_lst[count1]) # this is like this cuz i needed a list                                                                          
                    b=(current_subtrees_lst[count2]) # same as above                                                                                                  
                    #                    print a                                                                                                                                          
                    #                    print b                                                                                                                                          
                    a = a.replace('(','')
                    a = a.replace(')','')
                    a = a.replace(',',' ')
                    a = a.split(' ')
                    a1 = []
                    for i in a:
                        if i =='':
                            pass
                        else:
                            a1.append(i)
                    a = a1

                    b =b.replace('(','')
                    b = b.replace(')','')
                    b =b.replace(',',' ')
                    b =b.split(' ')
                    b1 = []
                    for j in b:
                        if j =='':
                            pass
                        else:
                            b1.append(j)
                    b = b1
                    new_tree_str='' # i dont no y this is here                                                                                                        
                    if set(a).intersection(set(b)):
                        #                        print 'BADDDDDDDDDDDDDDDDDD SWAP'
                        count2 +=1
                        #                        print a
                        #                        print b  
                        continue
    
                    else:
                        #                        print a
                        #                        print b   
                        prun_pt = current_subtrees_dic[current_subtrees_lst[count1]]
                        #                        print prun_pt             
                        cut_out = current_tree_dic[prun_pt].get_as_string()
                        base = current_tree_dic['Root'].get_chopped(prun_pt)
                        #                        print 'this is the treee ' + current_rerooted
                        #                        print 'this is cut out ' + cut_out
                        #                        print 'this is the base ' + base                                                                                                             
                        base_sub_dic = {}
                        if '(' in base and base != current_subtrees_lst[count2]:
                            t4 = Treecopy2.Tree(base,'n')
                            t1_dic = t4.container
                            for key in t1_dic.keys():
                                if key != 'n0' and key != 'Root':
                                    s = t1_dic[key].get_as_string()
                                    if s not in base_sub_dic.keys():
                                        base_sub_dic[s] =key

                            in_pt = current_subtrees_lst[count2]
                            #                            print 'this is base subtrees '
                            #                            print base_sub_dic.keys()
                            join_pt = base_sub_dic[in_pt]
                            insert_str = t1_dic[join_pt].get_as_string()
                            rest = t1_dic['Root'].get_chopped(join_pt)
                            new_tree_str = '((' + insert_str + ','+ cut_out + ')'+','+ rest + ')'

                            new_tree_like = like_ob.get_superlik(input_treeTaxonSet,new_tree_str)
                            #                            print current_tree_like   
                            #                            print new_tree_like 

#this next for loop is to reroot the new tree to the original root so that all the trees we are comparing have the same root
                            new_tree_str1 = None
                            new_tree_obj = Treecopy2.Tree(new_tree_str, 'w')
                            new_tree_dic=new_tree_obj.container
                            for i in new_tree_dic.keys():
                                if i != 'Root' and i != 'w0':
                                    if new_tree_dic[i].id == root:
                                        new_tree_str1 = new_tree_dic[i].Reroot()

                                
                            if float(new_tree_like) > float(current_tree_like):
#                                print' FOUND A NEW BETTERRRRRRRRRRRRRRRR TREE YAAAYYYYY'
                                current_tree = new_tree_str1
                                DidIJump = 'yes'
                                raise BreakOutOfForLoop4

                            else:
#                                print 'SHIOTEEEEEEE TREEEEEEE'
                                count2+=1
                                continue
                        else:
                            #                            print 'BADDDDDDDDDDDDDDDDDD SWAP AGAINNNNNNNNNNNNNNNN'
                            count2 +=1
                            continue

#                print 'GUESSSSSSSS WHO FINISHED COUNT1 WHILEEEEEEEEEEEEEE'
                count1 +=1
            current_rerooted1 = current_tree_dic[current_rerooting_pt].Reroot()
            if overall_like == None or current_tree_like > overall_like:
                overall_like = new_tree_like
                overall_best =[]
                overall_best.append(current_rerooted1)
            elif overall_like ==  current_tree_like:
                check = check_topology.is_newtopology(current_rerooted1,overall_best)
                if check != 'yes':
                    overall_best.append(current_rerooted1)

 
        except BreakOutOfForLoop4:
            continue
        #        print 'GUESSSSSSSS WHO FINISHED BIGCOUNT WHILEEEEEEEEEEEEEE'
        print overall_best
        bigcount +=1


elif spr_choice ==1:

    DidIJump = 'no' # checker flag... if we jumped cuz we found a better tree it is set to yes, if its cuz we have search all the rootin of a tree and nw want to go tru the trees to swap it set to mayb, if we have checked all tree and all rooting and we want to pick a new stpwise random tree it is set to no.
    current_tree = None
    current_tree_like = None
    bigcount = 0 # the very outer counter for the very outer while loop
    new_tree_like = None
    lagos = '' # will be used to store the new tree we get from the tree to swap list

    while bigcount <len(stepwise_tree_list):
#        print 'BACK AGAIN IN THE BIGCOUNT WHILE LOOOP '+ str(bigcount) + '\n'
#        print ' This are the trees in overall' + str(len(overall_best)) + '\n'
#        print overall_best
        if DidIJump =='yes':
#            print 'DIDIJUMP ==================================YES \n'
            
            current_tree = current_tree
            if overall_like == None or new_tree_like > overall_like:
                overall_like = new_tree_like
                overall_best =[]
                overall_best.append(current_tree)
            elif overall_like ==  new_tree_like:
                check = check_topology.is_newtopology(current_tree,overall_best)
                if check != 'yes':
                    overall_best.append(current_tree)
            trees_2_swap = {}
            trees_seen = []
        elif DidIJump == 'mayb':
#            print 'DIDIJUMP ==================================MAYBE \n'
            current_tree = lagos
        else:
            print 'Now in Iteration: ' + str(bigcount)
#            print 'DIDIJUMP ==================================NO \n'
            trees_2_swap = {}
            next_tree = stepwise_tree_list[bigcount]
            current_tree = next_tree
            trees_seen = []
        DidIJump = 'no'
        current_tree_ob = Treecopy2.Tree(current_tree, 'c')
        current_tree_dic = current_tree_ob.container
        current_root_pts=[]
        for key in current_tree_dic.keys():
            if key != 'c0' and key != 'Root':
                current_root_pts.append(key)
        root_pt = ''
#        print 'these is the number of rooting points' + str(len(current_root_pts))
        try:
#            print ' This are the trees in overall '
#            print overall_best
            for pt in current_root_pts:
                root_pt = pt
                current_rerooted = current_tree_dic[root_pt].Reroot()
        
                current_subtrees_lst = []
                current_subtrees_dic = {}
                
                current_rerooted_ob = Treecopy2.Tree(current_rerooted, 'r')
                current_rerooted_dic = current_rerooted_ob.container
                current_rerooting_pt = None
                for key in current_rerooted_dic.keys():
                    if key != 'r0' and key != 'Root':
                        s = current_rerooted_dic[key].get_as_string()
                        if current_rerooted_dic[key].id == root:
                            current_rerooting_pt = key
                        if s not in current_subtrees_lst:
                            current_subtrees_lst.append(s)
                            current_subtrees_dic[s] =key
                current_subtrees_lst = fisher_yates.shuffle(current_subtrees_lst)
                            
                current_tree_like = None
                like_ob = Supertree_likelihood.Supertree_likelihood()
                current_tree_like = like_ob.get_superlik(input_treeTaxonSet,current_rerooted) 
                new_tree_like = None
                count1 = 0
        
                while count1 < len(current_subtrees_lst):
#                    print 'COUNT1 WHILE AGAINNNNNNNNNNNN' +str(count1)
                    count2 = count1+1
                    
                    while count2 < len(current_subtrees_lst):
#                        print 'COUNT2 WHILE AGAINNNNNNNNNNNN' +str(count2)
                        a=(current_subtrees_lst[count1]) # this is like this cuz i needed a list 
                        b=(current_subtrees_lst[count2]) # same as above
                        #                    print a
                        #                    print b
                        a = a.replace('(','')
                        a = a.replace(')','')
                        a = a.replace(',',' ')
                        a = a.split(' ')
                        a1 = []
                        for i in a:
                            if i =='':
                                pass
                            else:
                                a1.append(i)
                        a = a1

                        b =b.replace('(','')
                        b = b.replace(')','')
                        b =b.replace(',',' ')
                        b =b.split(' ')
                        b1 = []
                        for j in b:
                            if j =='':
                                pass
                            else:
                                b1.append(j)
                        b = b1
                        new_tree_str='' # i dont no y this is here
                        if set(a).intersection(set(b)):
                            #                        print 'BADDDDDDDDDDDDDDDDDD SWAP'
                            count2 +=1
                            #                        print a
                            #                        print b
                            continue
                        else:
                            #                        print a
                            #                        print b
                            prun_pt = current_subtrees_dic[current_subtrees_lst[count1]]
                            #                        print prun_pt
                            cut_out = current_rerooted_dic[prun_pt].get_as_string()
                            base = current_rerooted_dic['Root'].get_chopped(prun_pt)
                            #                        print 'this is the treee ' + current_rerooted
                            #                        print 'this is cut out ' + cut_out
                            #                        print 'this is the base ' + base
                            base_sub_dic = {}
                            if '(' in base and base != current_subtrees_lst[count2]:
                                t4 = Treecopy2.Tree(base,'n')
                                t1_dic = t4.container
                                for key in t1_dic.keys():
                                    if key != 'n0' and key != 'Root':
                                        s = t1_dic[key].get_as_string()
                                        if s not in base_sub_dic.keys():
                                            base_sub_dic[s] =key

                                in_pt = current_subtrees_lst[count2] 
                                #                            print 'this is base subtrees ' 
                                #                            print base_sub_dic.keys()
                                join_pt = base_sub_dic[in_pt]
                                insert_str = t1_dic[join_pt].get_as_string()
                                rest = t1_dic['Root'].get_chopped(join_pt)
                                new_tree_str = '((' + insert_str + ','+ cut_out + ')'+','+ rest + ')'

                                new_tree_like = like_ob.get_superlik(input_treeTaxonSet,new_tree_str)
                        
                                #                            print current_tree_like
                                #                            print new_tree_like
                                
#this next for loop is to reroot the new tree to the original root so that all the trees we are comparing have the same root
                                new_tree_str1 = None

                                new_tree_obj = Treecopy2.Tree(new_tree_str, 'w')
                                new_tree_dic=new_tree_obj.container
                                for i in new_tree_dic.keys():
                                    if i != 'Root' and i != 'w0':
                                        if new_tree_dic[i].id == root:
                                            new_tree_str1 = new_tree_dic[i].Reroot()

                                if float(new_tree_like) > float(current_tree_like):
#                                    print' FOUND A NEW BETTERRRRRRRRRRRRRRRR TREE YAAAYYYYY'                                                         
                                    current_tree = new_tree_str1
                                    DidIJump = 'yes'
                                    raise BreakOutOfForLoop5

                                elif float(new_tree_like) == float(current_tree_like):
#                                    print 'SAME LIKEEEEEEEEEEEEEEEEEEEE' 
#                                    print 'NEW TREE IN THE SWAP LISTTTTTTTTTTTTT'
                                    check = check_topology.is_newtopology(new_tree_str1,trees_seen)
                                    if check != 'yes':
                                        check = check_topology.is_newtopology(new_tree_str1,trees_2_swap)
                                        if check !='yes':
                                            check = check_topology.is_newtopology(new_tree_str1,overall_best)
                                            if check !='yes':
                                                trees_2_swap[new_tree_str1] = new_tree_like
#                                                print 'SAME LIKEEEEEEEEEEEEEEEEEEEE' + str(len(trees_2_swap.keys()))
                                    count2+=1
                                    continue
                                elif float(new_tree_like) < float(current_tree_like):
                                    count2+=1
                                    continue
                            else:
                                #                            print 'BADDDDDDDDDDDDDDDDDD SWAP AGAINNNNNNNNNNNNNNNN'                                   
                                count2 +=1
                                continue
#                        print 'GUESSSSSSSS WHO FINISHED COUNT2 WHILEEEEEEEEEEEEEE'                                                                   
                        count2 +=1
#                    print 'GUESSSSSSSS WHO FINISHED COUNT1 WHILEEEEEEEEEEEEEE'                                                                       
                    count1 +=1
                current_rerooted1 = current_rerooted_dic[current_rerooting_pt].Reroot()
                if overall_like == None or current_tree_like > overall_like:
                    overall_like = new_tree_like
                    overall_best =[]
                    overall_best.append(current_rerooted1)
                elif overall_like ==  current_tree_like:
                    check = check_topology.is_newtopology(current_rerooted1,overall_best)
                    if check != 'yes':
                        overall_best.append(current_rerooted1)
            
        except BreakOutOfForLoop5:
            continue
        if len(trees_2_swap.keys()) != 0:
            print 'Now in the SWAP LIST. This is the number of trees in Swap list:' + str(len(trees_2_swap.keys())) + '\n'
            nu_tuple = trees_2_swap.popitem()
            lagos = nu_tuple[0]
            trees_seen.append(lagos)
            DidIJump= 'mayb'
            continue
   #     print 'GUESSSSSSSS WHO FINISHED BIGCOUNT WHILEEEEEEEEEEEEEE'
        print overall_best
        bigcount +=1


t1 = time()        
outf.write('\nmethod ' + str(spr_choice)+ "\t" + 'trees: ' + "\t" + str(len(overall_best))+ "\t" + 'like: ' + "\t" + str(overall_like) + "\t" + 'time: ' + "\t" +str(t1-t0) + "\n")
for i in overall_best:
    i = i.replace(' ', '')
    outf.write(i + ';' + "\n") 
outf.close()
