"""                                                                                                                                                                                  
Created by Wasiu Ajenifuja Akanni on 2013.                                                                                                                                           
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.                                                                                                                      
"""

import dendropy

import sys
import os.path
from optparse import OptionParser
parser = OptionParser()
print " -This script offers the capability to deroot rooted trees. This script requires the DENDROPY package to be installed \n "

parser.add_option("-i", "--input_treefile", action="store", dest="input_file", help="The name of the file containing your list of newick formatted trees that you want to deroot: ", metavar="genetreeFILE")

parser.add_option("-o", "--output_file", action="store", dest="outfile", help=" The name of the file the derooted trees are to be printed into :  Default= Deroot_output " , default= "Deroot_output")

options, args = parser.parse_args()
input_f = options.input_file
if not os.path.exists(input_f):
    print "The input tree file is not a file or does not exist "
    sys.exit(1)

tree_file = open(input_f , 'r')

tree_lst = tree_file.readlines()
d =[]

for i in tree_lst:
    tree = dendropy.Tree.get_from_string(i, "newick")
    tree.is_rooted = False
    tree.update_splits()
    s = tree.as_string('newick')
    d.append(s)

outfile = options.outfile
outp= open(outfile, 'a')
for t in d:
    t = t.replace("[&U] ","")
    outp.write(t)

print "finito!!!\n"
