"""
Created by Wasiu Ajenifuja Akanni on 2013.
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.
"""

print 'This script takes in a file containing a list of trees in newick format and writes out their combined taxon set into a file specified by the user \n'


import sys
import os.path
sys.path.append('src')
from ExtractTaxa import *
from optparse import OptionParser
parser = OptionParser()

parser.add_option("-i", "--input_treefile", action="store", dest="input_file", help="The name of the file containing your list of newick formatted trees: ", metavar="genetreeFILE")

parser.add_option("-o", "--output_file", action="store", dest="outfile", help=" The name of the file the results are to be printed into :  Default= ExtractTaxon_file_output " , default= "ExtractTaxon_file_output")

options, args = parser.parse_args()
input_f = options.input_file
if not os.path.exists(input_f):
    print "The input tree file is not a file or does not exist "
    sys.exit(1)


outfile =options.outfile
input_obj = open(input_f, 'r')
input_lst = input_obj.readlines()

out_obj = open(outfile,'a')

taxa_lst=maktaxon_list(input_lst)

for i in taxa_lst:
    out_obj.write(i + "\n")

input_obj.close()
out_obj.close()
print 'Done!!!!!!!!!!!!!!!!'

