"""
Created by Wasiu Ajenifuja Akanni on 2013. modified 2014
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.
"""

import sys
import os.path
sys.path.append('src')
import parse_newick
print "This method can be used to convert a nexus formated trees to altnexus or newick"
print "It requires two input file to function properly"
print "The first input file should contain just your nexus formated trees without the taxa block or anything else. remove the 'end;' an example of these can be found in the Example folder under 'NexusExample_file.nex' "
print "The second input file should contain the list of taxa in the same order as they in the nexus file"
from optparse import OptionParser

def change_back(in_list, taxon_list):
#    print "This method can be used to convert a nexus formated trees to altnexus or newick"                                                                           # this method uses the taxon list (the list of taxa in the same order as they in the nexus file) to change the taxon names back from the index numbers to the original taxon names               
    input_list= []# this list contains the input trees now change bak to their actual taxon names    
    parser = parse_newick.parse_newick() # create an object of the parse_newick class  
    for i in in_list:
#        print "DEBUG: This is the nexus tree : ", i
        word = ''
        tree = parser.parse_newick(i)
        for j in tree:
            p = j.isdigit()
            k = j
            if p:
                j = int(j)
                k = taxon_list[j-1]
            temp = ''.join(str(k).split())
            word = word + str(temp)
#        print "DEBUG: This is the newick tree : " , word
        input_list.append(word)
    return input_list








parser = OptionParser()
parser.add_option("-i", "--input_treefile", action="store", dest="input_file", help="The name of the file containing the trees in nexus format", metavar="genetreeFILE")
parser.add_option("-r", "--taxa_file", action="store", dest="taxa_file", help="The name of the file containing the taxon list ", metavar="taxaFILE")
parser.add_option("-o", "--output_file", action="store", dest="outfile", help=" The name of the file that you want the newick formatted trees to be written. Default= Convert2Newick_output " , default= "Convert2Newick_output")

options, args = parser.parse_args()

input_f = options.input_file
if not os.path.exists(input_f):
    print "The input tree file is not a file or does not exist "
    sys.exit(1)
in_f = open(input_f, 'r')
input_trees=in_f.readlines()
in_f.close()

input_f2 =options.taxa_file
if not os.path.isfile(input_f2):
    print "The input taxa file is not a file or does not exist "
    sys.exit(1)
in_f2 = open(input_f2, 'r')
input_taxa=in_f2.readlines()

newick = change_back(input_trees,input_taxa)

outf = options.outfile
outfile = open(outf, "a")
for item in newick:
  outfile.write("%s;\n" % item)

outfile.close()

print "FINISHED!!!"

