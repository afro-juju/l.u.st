"""
Created by Wasiu Ajenifuja Akanni on 2013.
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.
"""

import sys
import os.path
sys.path.append('src')
import dendropy
import Treecopy2
import Node
import parse_newick
import Prune
from ExtractTaxa import *
print "This script takes as input a file containing a list of newick formatted polytomous gene trees and a resolved supertree and uses the supertree to  resolve the clades in the gene trees. \n THIS METHOD REQUIRES DENDROPY PACKAGE TO BE INSTALLED. \n "

from optparse import OptionParser
parser = OptionParser()
parser.add_option("-i", "--input_treefile", action="store", dest="input_file", help="The name of the file containing the unresolved trees in newick format", metavar="genetreeFILE")
parser.add_option("-r", "--resolved_treefile", action="store", dest="resolved_file", help="The name of the file containing the resolved tree in newick format *** this must be fully resolved", metavar="resolvedtreeFILE")
parser.add_option("-o", "--output_file", action="store", dest="outfile", help=" The name of the file that you want the resolved trees to be written. Default= Resolve_phylogenies_output " , default= "Resolve_phylogenies_output")

options, args = parser.parse_args()

input_f = options.input_file
if not os.path.exists(input_f):
    print "ERROR: The input file given is not a file or it is doesn't exit "
    sys.exit(1)

in_f = open(input_f, 'r')
input_trees=in_f.readlines()
in_f.close()

input_f2 =options.resolved_file
if not os.path.exists(input_f2):
    print "ERROR: The supertree file given is not a file or it is doesn't exit "
    sys.exit(1)
in_f2 = open(input_f2, 'r')
input_supertree=in_f2.readlines()
if set(maktaxon_list(input_trees)) != set(maktaxon_list(input_supertree)) and set(maktaxon_list(input_trees)).issubset(set(maktaxon_list(input_supertree))) == False:
    print "ERROR: The supertree taxon set  does not match the input tree union of taxon set "
    print list(set(maktaxon_list(input_trees))-set(maktaxon_list(input_supertree)))
    print list(set(maktaxon_list(input_supertree))-set(maktaxon_list(input_trees)))
    print len(maktaxon_list(input_trees))
    print len(maktaxon_list(input_supertree))
    sys.exit(1)

supertree=input_supertree[0]
supertree=supertree.replace(' ','')
in_f2.close()

out_f =options.outfile
flag1 = 'no'
temp_list = []
prune_object = Prune.Prune()
p=parse_newick.parse_newick()

seen_polytomies = []
def is_polyNode(node):
    flag = False

    if len(node.daughters.keys()) >= 3:
        flag = True

    return flag


#i had to create a recursive method to deal with a tree with multiple polytomies
def resolve(tree1,super1):
    global seen_polytomies
    flag1 = 'no'
    tree1=tree1.replace(' ','')
    treeA = Treecopy2.Tree(tree1,'A')
    t_dic=treeA.container
    poly_node = ''
    working = ''
    cut = ''
    for i in t_dic.keys():
#        if i != 'A0' and i !='Root': #this if statement was added to deal with fully resolved bifurcating trees, as their roots nodes have 3 children. will probably find a better way to deal with this. will talk to davide. ** commented out now that we are rooting the trees

        if len(t_dic[i].daughters.keys()) >= 3:
#            print 'polytomous node: ' + str(i)
            for n in t_dic[i].daughters.keys():
                f = is_polyNode(t_dic[i].daughters[n])
                if f: 
                    cut=t_dic[n].get_as_string().replace(' ','')
                    if cut in seen_polytomies:
#                        print "DEBUG: RESOLVE: seen this polytomy before and it is also unresolved in the supertree" , cut
                        continue
                    else:
                        poly_node = n
#                        print "DEBUG: RESOLVE: n is the polynode ", t_dic[n].id
                        break
                else:
                    continue
            if poly_node == '':
                cut=t_dic[i].get_as_string().replace(' ','')
                if cut in seen_polytomies:
#                    print "DEBUG: RESOLVE: seen this polytomy before and it is also unresolved in the supertree" , cut
                    continue
                else:
                    poly_node = i
            break
    if poly_node == '': #added to make sure the tree actually contains a polytomy
#        print 'This tree is resolved ' + tree1
        working = tree1
    else:
#        cut=t_dic[poly_node].get_as_string().replace(' ','')
#        print 'DEBUG RP: this is the polytomous subtree ' + cut
        cut_ob = Treecopy2.Tree(cut, 'T')
        cut_dic = cut_ob.container
        cut_taxonSet=[]
#the for loop below gives me a list of all the possible rooting position for the current tree   
        for key in cut_dic.keys():
            if cut_dic[key].id != None:
                cut_taxonSet.append(cut_dic[key].id)

#prune the supertree of all other taxa except for those in 'cut'
        pruned_super=prune_object.Prune(cut,cut_taxonSet,super1)
        pruned_super=pruned_super.replace(' ','')
#        print 'DEBUG RP: this is prunned super ' + pruned_super
        if sorted(list(cut))!= sorted(list(pruned_super)):#checking to make sure that the polytomy is resolved in the supertree.   
            taxa2prune = cut_taxonSet
            working_cp = tree1 #a copy of the tree for the prunning step
#the next step is to go tru the list and extract the taxa name from list, use the name to get the node identifier then use the identifier to prune that taxa from the tree. 
            for name in taxa2prune:
                if name != ',' and name != ')' and name !='(' and name != ';':
#                print 'This is working copy: ' + working_cp
                    if ',' in working_cp: #Added to ensure we dont get a error when deleting all last taxa left in a tree.
                        wt = Treecopy2.Tree(working_cp, 'W')
                        wd=wt.container
                        for j in wd.keys():
                            if wd[j].id == name.strip():
                                working_cp=wd['Root'].get_chopped(j)
                                working_cp=working_cp.replace(' ','')
                    else:
                        working_cp = ''
#                    print 'DEBUG: everything is polytomous '
                        break
#        print 'DEBUG: this is working cp ' + working_cp
        
            if working_cp == '':
                working = pruned_super + ';'
                working=working.replace(',)',')')
            else:
                working = '('  + working_cp + ',' + pruned_super + ')' + ';'
                working=working.replace(',)',')')
        else:#if the polytomy we want to resolve is not resolved in the supertree then we add it the list of polytomies that are not resolved and we move on to the nx\t polytomy    
            seen_polytomies.append(cut)
#            print "DEBUGGGGGGGGGGGGGG: resolve: adding this to the seen polytomies list::::  \n" , cut
            return resolve(tree1,super1)
    flag1=is_polytomous(working) #checks if there are still any polytomies left in the tree 
    if flag1=='yes' and tree1 != working: #I had to add the ' and tree1 != working:' bit to deal with when there is a polytomy in the supertree itself at this node. so if we get the same polytomy twice we can stop the search and not end up in a loop
#        print 'MOTHERFUCKAAAAAAAAA ' + working

#        print 'DEBUG RP: This is working with a polytomy that is yet to be resolved ' +working
        return resolve(working,super1)
    else:
        return working

#this is to check if a tree has polytomies. i had to include to deal with cases whereby the polytomy that we resolved in the tree itself is inside another polytomy. Hence trees with more than one polytomy
def is_polytomous(t):
    check = 'no' 
    t_obj = Treecopy2.Tree(t,'B')
    t_d=t_obj.container
    for i2 in t_d.keys():
#        if i2 != 'B0' and i2 !='Root': #this if statement was added to deal with fully resolved bifurcating trees, as their roots nodes have 3 children. will probably find a better way to deal with this. will talk to davide. *** commented out because i found a way to root the tree!!
        if len(t_d[i2].daughters.keys()) >= 3:
            check='yes'
            break

    return check

tree_obj = Treecopy2.Tree(supertree, 'w')
tree_dic=tree_obj.container
super_str1=''
super_root=''
#print 'DEBUG RP: UNRooted SUPERTREE - ' +supertree
    #First i use this for loop to identify the taxa we want to reroot this tree at                                                                                                 
for i in tree_dic.keys():
    if i != 'Root' and i != 'w0':
        if tree_dic[i].id != None:
            super_root= tree_dic[i].id
            break
super_root=super_root.replace('_',' ') #This was added because dendropy automatically comverts _ in the taxa names to spaces. so to match the taxa i needed to change the _ to a space                                                                                                                                                                              
#print super_root
    #Next i use dendropy to reroot the tree at the above taxa. we would have used my own rooting script but my Tree class does not deal with polytomous well 
supertree1 = dendropy.Tree.get_from_string(supertree, "newick")
root_super = supertree1.find_node_with_taxon_label(super_root)
supertree1.reroot_at_edge(root_super.edge, update_splits=False)
super_str1 = supertree1.as_string('newick')
super_str2=super_str1.replace('[&R] ','')
#print 'DEBUG RP: ROOTED SUPERTREE - ' +super_str2


for tree in input_trees:
    tree_obj = Treecopy2.Tree(tree, 'w')
    tree_dic=tree_obj.container
    tree_str1=''
    nu_root=''
#    print 'DEBUG RP: UNRooted TREE - ' +tree
    #First i use this for loop to identify the taxa we want to reroot this tree at 
    for i in tree_dic.keys():
        if i != 'Root' and i != 'w0':
            if tree_dic[i].id != None:
                nu_root= tree_dic[i].id
                break
    nu_root=nu_root.replace('_',' ') #This was added because dendropy automatically comverts _ in the taxa names to spaces. so to match the taxa i needed to change the _ to a space
#    print nu_root  
    #Next i use dendropy to reroot the tree at the above taxa. we would have used my own rooting script but my Tree class does not deal with polytomous well
    tree1 = dendropy.Tree.get_from_string(tree, "newick")
    root_node = tree1.find_node_with_taxon_label(nu_root)
    tree1.reroot_at_edge(root_node.edge, update_splits=False)
    tree_str1 = tree1.as_string('newick')
    tree_str2=tree_str1.replace('[&R] ','')
#    print 'DEBUG RP: UNRESOLVED TREE - ' +tree_str2
    tree_r=resolve(tree_str2,super_str2)

    if set(maktaxon_list(list(tree))) != set(maktaxon_list(list(tree_r))):
        print "ERROR: There has been a problem during resolution \n The tree and the resolved tree dont have the same taxon set "
        print 'DEBUG RP: UNRESOLVED TREE - ' +tree_str2
        print 'DEBUG RP: RESOLVED TREE - ' +tree_r
        sys.exit(1)

#    print 'DEBUG RP: RESOLVED TREE - ' +tree_r 
#    print 'DEBUG RP: LEN OF TEMP LIST ' + str(len(temp_list))
    temp_list.append(tree_r)

outp = open(out_f,'w')
outp.write('\n'.join(str(x).rstrip('\n\r ') for x in temp_list))
print "finito: output written to the file - " + out_f
outp.close()
