#### COMPLETED ON 08/11/2013
"""
Created by Wasiu Ajenifuja Akanni on 2013.
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.
"""

from __future__ import division
import sys 
sys.path.append('src')
import math
import Treecopy2
import Prune
import Sym_diff
import Supertree_likelihood
print " This script calculates the winning site test for two supertrees. \n It takes as input a file containing your supertree A, file containing supertree B, and a file containing the input gene trees from which they had been inffered. \n all trees should be in newick format! \n "

from optparse import OptionParser
parser = OptionParser()
parser.add_option("-a", "--ml_treefile", action="store", dest="ml_file", help="The name the file containing your ml supertree in newick format", metavar="supertreeAFILE")
parser.add_option("-b", "--other_treefile", action="store", dest="super_file", help="The name the file containing your the other supertree in newick format", metavar="othertreeAFILE")
parser.add_option("-i", "--in_treefile", action="store", dest="gene_treefile", help="The name the file containing your list of input gene trees in newick format", metavar="input_treeFILE")
parser.add_option("-m", "--ML_output", action="store", dest="outfile1", help=" The name of the file where the input like value of ml supertree is to be stored. Default= Mlsupertree_likelihood " , default= "Mlsupertree_likelihood")
parser.add_option("-s", "--other_Super_output", action="store", dest="outfile2", help=" The name of the file where the input like value of the other supertree is to be stored. Default= other_supertree_likelihood " , default= "other_supertree_likelihood")
parser.add_option("-w", "--winning_output", action="store", dest="outfile3", help=" The name of the file where the comparison -,+,zero is  to be stored. Default= winning_site1_output " , default= "winning_site1_output")
options, args = parser.parse_args()

supertreeA = options.ml_file
proposed_treeA = open(supertreeA, 'r') # Open a file with predefined newick supertrees (just for testing this will be deleted) 
supertreeB =options.super_file
proposed_treeB = open(supertreeB, 'r') # Open a file with predefined newick supertrees (just for testing this will be deleted)           

input_list1 = options.gene_treefile
input_list2 = open(input_list1, 'r')
input_list = input_list2.readlines() # puts every thing in the file into a list
#tlst = dendropy.TreeList.get_from_path(input_list, 'newick')  # This is a list of dendropy tree objects
counter = len(input_list) # dimension of input list counting from 1                                                         
print '###################'
print counter
print '###################'
count = 0
output_file = options.outfile1
input_liks = open(output_file , 'w')
output_file1 = options.outfile2
input_liks1 = open(output_file1 , 'w')
output_file2 =options.outfile3
input_liks2 = open(output_file2 , 'w')

def get_like(alpha,beta,distance):

    delta = distance   # Normalised tree-to-tree distance.  THis is currently Normalised RF but can take any distane (e.g. quartet)
    bd = beta * delta * -1  # remember the -1 might not work AJ.  Test it!  
    exp_bd = math.exp(bd)
    like= (alpha * exp_bd)
    like = math.log10(like)
    return like


def test_cal(no_plus, no_minus):
    expected = no_plus + no_minus
    expected = expected /2
    ptA = no_plus - expected
    ptA = ptA * ptA
    
    ptB = no_minus - expected
    ptB = ptB * ptB
    
    ans = ptA + ptB
    if ans == 0:
        print ' Can not calculate winning site as both supertrees have the same likelihood \n'
    else:
        answer = ans / expected
        return answer

likezA = []
likezB = []

comparisons = []

for proposed_strA in proposed_treeA:  # loop through supertrees
    
    print ' :-):-):-):-):-):-):-):-)'
    non_normalised_distA = []
    input_counter = 0
    
    while input_counter < counter:
#        print 'now in the while loop'
        input_treeA = input_list[input_counter]  # This takes newick input tree from the list               
#        nuproposed_tree = Treecopy2.Tree(proposed_str, 'P') # Make an object of current propose supertree  

        input_tree1= Treecopy2.Tree(input_treeA, 'I')  # we need here to remake an object becaus dendropy does not make taxon sets of individual tree (suboptimal but could not think anything better).                           \
                          
        input_dic = input_tree1.container
        no_of_inputtaxa = 0
        inputTree_taxa = []
        for i in input_dic.keys():
            if input_dic[i].id != None:
                no_of_inputtaxa +=1
                inputTree_taxa.append(input_dic[i].id)
        prune_object = Prune.Prune()

#        print 'this is the input tree we sending in ' + input_treeA
#        print no_of_inputtaxa                                                                                                                                                                                                    
#        print 'this is the supertree we are sending in ' + proposed_strA
        pruned_supertree = prune_object.Prune(input_treeA,inputTree_taxa,proposed_strA)
#        print ' tis is the out put from prune.py ' + pruned_supertree
        rf_object = Sym_diff.sym_diff()
        distance = rf_object.get_sym_diff(pruned_supertree,input_treeA)
        #            normal_dist = (distance)/(((2* no_of_inputtaxa)-6))
        #        print 'this is distance '                                                                       

        alpha = 1
        beta = 1

        like = get_like(alpha,beta,distance)

        likezA.append(like)

        non_normalised_distA.append(distance)

        input_counter +=1

for proposed_strB in proposed_treeB:  # loop through supertrees                                         

    print ' :-):-):-):-):-):-):-):-)'
    non_normalised_distB = []
    input_counter = 0

    while input_counter < counter:
#        print 'now in the while loop'                                                                  
        input_treeB = input_list[input_counter]           
#        nuproposed_tree = Treecopy2.Tree(proposed_str, 'P') # Make an object of current propose supertree        
        input_tree1= Treecopy2.Tree(input_treeB, 'W')  # we need here to remake an object becaus dendropy does notmake taxon sets of individual tree (suboptimal but could not think anything better).                       

        input_dic = input_tree1.container
        no_of_inputtaxa = 0
        inputTree_taxa=[]
        for i in input_dic.keys():
            if input_dic[i].id != None:
                no_of_inputtaxa +=1
                inputTree_taxa.append(input_dic[i].id)
        prune_object = Prune.Prune()

#        print 'this is the input tree we sending in ' + input_treeB
#        print no_of_inputtaxa                                                                    
                                                                                                                   
#        print 'this is the supertree we are sending in ' + proposed_strB
        pruned_supertreeB = prune_object.Prune(input_treeB,inputTree_taxa,proposed_strB)
#        print ' tis is the out put from prune.py ' + pruned_supertreeB
        rf_object = Sym_diff.sym_diff()
        distance = rf_object.get_sym_diff(pruned_supertreeB,input_treeB)
        #            normal_dist = (distance)/(((2* no_of_inputtaxa)-6))                                           
        #        print 'this is distance '                                                                         
        alpha = 1

        beta = 1

        like = get_like(alpha,beta,distance)

        likezB.append(like)

        non_normalised_distB.append(distance)

        input_counter +=1





for j in likezA:
    input_liks.write(str(j) + "\n")
for p in likezB:
    input_liks1.write(str(p) + "\n")


cunt = 0
plus = "+"
minus = "-"
no_plus = 0
no_minus = 0
no_zero = 0
while cunt< len(likezA):
    first = likezA[cunt]
    first = float(first)
    second =likezB[cunt]
    second = float(second)
#    print 'this is firsttttttttttttttttttttttttttttt'
#    print first
#    print 'this is secondddddddddddddddddddddddddddddd'
#    print second
    if first == second:
        no_zero +=1
        input_liks2.write('zero' + '\n')
#        print str(first) + ' is the same as ' + str(second)
    elif first > second:
        no_plus +=1
        input_liks2.write(plus + "\n")
#        print str(first) + ' is bigger than ' + str(second)
    elif first < second:
        no_minus +=1
        input_liks2.write(minus + "\n")
#        print str(first) + ' is lesserrrrrr than ' + str(second)
    else:
        input_liks2.write('WTF' +'\n')
#        print 'boooooooboooooooooo'

    cunt +=1

print 'this is the no of pluses ' + str(no_plus)
input_liks2.write('this is the nunber of pluses ' + str(no_plus) +"\n")
print 'this is the no of minuses ' + str(no_minus)
input_liks2.write('this is the number of minuses ' + str(no_minus) +"\n")
print 'this is the no of zeros ' + str(no_zero)
input_liks2.write('this is the number of zeros ' + str(no_zero) +"\n")
winning_site= test_cal(no_plus , no_minus)
print '#################################'
print '#################################'
print 'this is the result of the WINNING SITE TEST ' + str(winning_site)
input_liks2.write('this is the result of the WINNING SITE TEST ' + str(winning_site) +"\n")
print '################################'
print '################################'

print 'finito'
input_liks.close()
input_liks1.close()
input_liks2.close()
proposed_treeA.close()
proposed_treeB.close()
