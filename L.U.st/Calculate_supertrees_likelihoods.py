"""                                                                                                                                                                                  
Created by Wasiu Ajenifuja Akanni on 2013.                                                                                                                                           
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.                                                                                                                      
"""
from __future__ import division
import math
import sys
sys.path.append('src')
import Treecopy2
import Prune
import Sym_diff
print " THIS CODE TAKES AS INPUT A SET OF INPUT TREES AND ALL THE POSSIBLE SUPERTREES ON THE TAXON SET OF THE INPUT TREES.. IT RETURNS A FILE COUNTAINING THE SYM DIFF AND THE LIKELIHOOD VALUES FOR EACH OF THE SUPERTREES. IT USES my own code!!! \n"

import sys
import os.path
from optparse import OptionParser
parser = OptionParser()

parser.add_option("-i", "--input_treefile", action="store", dest="input_file", help="The name of the file containing your list of newick formatted input trees: ", metavar="genetreeFILE")
parser.add_option("-s", "--super_file", action="store", dest="Stree_file", help="The name of the file containing the supertree list in newick format ", metavar="taxaFILE")
parser.add_option("-o", "--output_file", action="store", dest="outfile", help=" The name of the file where the ML values and the min RF values are to be stored: Default= STree_likelihood_output " , default= "STree_likelihood_output")

options, args = parser.parse_args()
input_f = options.input_file
if not os.path.exists(input_f):
    print "The input tree file is not a file or does not exist "
    sys.exit(1)

STree_f = options.Stree_file
if not os.path.exists(STree_f):
    print "The input tree file is not a file or does not exist "
    sys.exit(1)

proposed_tree = open(STree_f, 'r') # Open a file with predefined newick supertrees (just for testing this will be deleted)
tlst1 = open(input_f, 'r')
input_list = tlst1.readlines() 
counter = len(input_list) # dimension of tlist counting from 1               
count = 0
outfile=options.outfile
out_line = open(outfile, 'w') # output p files (will also be changed toallow user specific names)
out_line.write("TreeNo. \t Sumd(Dist) \t Sumd(P)\n") #header for values file
#input_file = input('give the name of the file where the the input like value is to be stored')
#input_liks = open(input_file , 'w')

def get_like(alpha,beta,distance):

    delta = distance   # un-normalised tree-to-tree distance.  THis is currently un_normalised RF but can take any distane (e.g. quartet)
    bd = beta * delta * -1  # remember the -1 might not work AJ.  Test it!                                        
    exp_bd = math.exp(bd)
    like= (alpha * exp_bd)
    like = math.log10(like)
    return like

for supertree in proposed_tree:  # loop through supertrees                                                       
#    print 'DEBUG1!!!! current supertree '+supertree
    
    input_counter = 0
    counter = len(input_list)
    likez = []
    dist =[]
    prune_object = Prune.Prune()
    rf_object = Sym_diff.sym_diff()
    non_normalised_dist = []
    while input_counter < counter:
        #            print 'now in the while loop'
        input_tree = input_list[input_counter]

        inputTree_taxa=[]
        input_tree1= Treecopy2.Tree(input_tree, 'I')                          
        input_dic = input_tree1.container
#        no_of_inputtaxa = 0
        for i in input_dic.keys():
            if input_dic[i].id != None:
#                no_of_inputtaxa +=1
                inputTree_taxa.append(input_dic[i].id)
#        print 'this is the input tree we sending in ' + input_tree
#        print no_of_inputtaxa                                               
#        print 'this is the supertree we are sending in ' + supertree
        pruned_supertree = prune_object.Prune(input_tree,inputTree_taxa,supertree)
#        print ' tis is the out put from prune.py ' + pruned_supertree
        distance = rf_object.get_sym_diff(input_tree,pruned_supertree)
#        normal_dist = distance/((2* no_of_inputtaxa)-6)
#            normal_dist = normal_dist + 1
#            normal_dist = normal_dist * -1
#        print 'this is distance '                                                                                                                                                                                                
#        print 'DEBUG2!!! ' + str(distance)                                                                                                                                
        alpha = 1
        beta = 1

        like = get_like(alpha,beta,distance)
#        print'DEBUG12!!! input likelihood '+ str(like)  

        likez.append(like)
        #            dist.append(normal_dist)
        non_normalised_dist.append(distance)

        input_counter +=1

#    cunt = 0
#    for  j in likez:
#        input_liks.write(str(cunt) + "\t" + "\t" + str(j) + "\n")
#        cunt += 1
    like_current_supertree = sum(likez)
#    sumdist = sum(dist)
    unsumdist = sum(non_normalised_dist)
#    print'DEBUG3!!! supertree likelihood ' + str(like_current_supertree)
#    print 'DEBUG5!!! unnomalised dist. ' + str(unsumdist)

#    print'DEBUG6!!! counter ' +str(count)
    nu_count = count * 100
    out_line.write(str(nu_count) + "\t"+"\t" + str(unsumdist) + "\t" +  "\t" + str(like_current_supertree) + "\n")

    count += 1
print 'finito'
#input_liks.close()
proposed_tree.close()
out_line.close()

