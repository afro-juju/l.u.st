"""
Created by Wasiu Ajenifuja Akanni on 2013.
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.
"""
import sys
sys.path.append('src')
import os.path
import pseudo_dataset_latest

from optparse import OptionParser
parser = OptionParser()

parser.add_option("-i", "--input_treefile", action="store", dest="input_file", help="The name of the file containing your list of newick formatted trees: ", metavar="genetreeFILE")
parser.add_option("-r", "--rep_num", type= "int", action="store", dest="rep_no", help=" The bootstrap replicate number : " , metavar="replicateNumber")
parser.add_option("-o", "--output_file", action="store", dest="outfile", help=" The name of the file the results are to be printed into :  Default= Bootstrap_replicate_output " , default= "Bootstrap_replicate_output")

options, args = parser.parse_args()
input1 = options.input_file
if not os.path.exists(input1):
    print "The input tree file is not a file or does not exist "
    sys.exit(1)

input_trees_file = open(input1, 'r')
input_trees_list = input_trees_file.readlines()
out1 = options.outfile
replicate = options.rep_no
ob = pseudo_dataset_latest.pseudo_dataset()

new_datasets = ob.get_pseudo_supertrees(input_trees_list,replicate)
count = 0
for i in new_datasets:
    out2=out1 + '_' + str(count)
    f= open(out2,'a')
    for j in i:
        f.write(j)
    f.close()
    count +=1
