"""
Created by Wasiu Ajenifuja Akanni on 2013. Modified 2014.
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.
"""

from optparse import OptionParser
import sys 
import os.path

print 'This script takes a file of newick formatted trees and changes them to altnexus formattedtrees \n '

parser = OptionParser()
parser.add_option("-i", "--input_treefile", action="store", dest="input_file", help="The name of the file containing the trees in newick format", metavar="newictreeFILE")
parser.add_option("-o", "--output_file", action="store", dest="outfile", help=" The name of the file that you want the nexus formatted trees to be written. Default= Convert2Nexus_output " , default= "Convert2Nexus_output")

options, args = parser.parse_args()




input_file = options.input_file
if not os.path.exists(input_file):
    print "The input tree file is not a file or does not exist "
    sys.exit(1)
out_file = options.outfile
input_obj= open(input_file, 'r')
input_trees= input_obj.readlines()
out_obj=open(out_file, 'a')
out_obj.write("#nexus \n")
out_obj.write("begin trees;\n")
count = 0

while count < len(input_trees):
    tree = 'tree PAUP_' +str(count)+ ' = [&U] '+ input_trees[count]
    out_obj.write(tree)
    count +=1
out_obj.write("\nend;\n")
input_obj.close()
out_obj.close()

print "FINISHED!!!!"
