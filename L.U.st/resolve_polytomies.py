"""
Created by Wasiu Ajenifuja Akanni on 2013.
Copyright (c) 2013 Wasiu Ajenifuja Akanni. All rights reserved.
"""

### THIS SCRIPT WILL GENERATE 1 RANDOM RESOLUTION OF EVERY POLYTOMOUS TREE ITS GIVEN
from cStringIO import StringIO
from dendropy import Tree, TaxonSet
import sys
import os.path

print "This will randomly resolve a polytomous tree. It requires the dendropy package to be installed. \n"

from optparse import OptionParser
parser = OptionParser()
parser.add_option("-i", "--input_treefile", action="store", dest="input_file", help="The name of the file containing the unresolved trees in newick format", metavar="genetreeFILE")

parser.add_option("-o", "--output_file", action="store", dest="outfile", help=" The name of the file that you want the resolved trees to be written. Default= Resolve_phylogenies_output " , default= "Resolve_polytomies_output")

options, args = parser.parse_args()

input_f = options.input_file
if not os.path.exists(input_f):
    print "Error: The input file given is not a file or doesnt exit "
    sys.exit(1)

source = open(input_f,'r') #input file of newick formated trees
count = 0
tree_list = source.readlines()
file_name = options.outfile #the output file
file = open(file_name , 'a')
for i in tree_list:
    cunt = 1
    while cunt != 0:
        tree = Tree.get_from_string(i , "newick", tree_offset=0)
        tree.resolve_polytomies()
        string1 = tree.as_string(schema="newick")
        string1 = string1.replace("[&U] ","")
        file.write(string1)
        cunt -=1
    count +=1
print count
file.close()
