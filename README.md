To download the latest version of the package simply copy and paste the clone link in the HTTPS box above or from below into your terminal window and hit enter. All necessary files including the examples will be downloaded to your current working directory under the name l.u.st. Once downloaded the package is ready to be used, no installation is needed.

https://afro-juju@bitbucket.org/afro-juju/l.u.st.git

The latest version of the manual can be downloaded by clicking the Downloads link to the left and clicking on the manual version that you want.

If you run into any problems feel free to contact me:

waakanni13@gmail.com